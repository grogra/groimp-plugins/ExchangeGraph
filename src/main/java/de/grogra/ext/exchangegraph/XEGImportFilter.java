
/*
 * Copyright (C) 2002 - 2007 Lehrstuhl Grafische Systeme, BTU Cottbus
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package de.grogra.ext.exchangegraph;

import java.io.Reader;
import java.util.HashMap;
import java.io.IOException;

import de.grogra.graph.impl.Node;
import de.grogra.pf.io.FileSource;
import de.grogra.pf.io.FilterBase;
import de.grogra.pf.io.FilterItem;
import de.grogra.pf.io.FilterSource;
import de.grogra.pf.io.IOFlavor;
import de.grogra.pf.io.ObjectSource;
import de.grogra.pf.registry.Item;
import de.grogra.pf.registry.ItemCriterion;
import de.grogra.pf.registry.TypeItem;
import de.grogra.pf.ui.Workbench;
import de.grogra.reflect.Type;

public class XEGImportFilter extends FilterBase implements ObjectSource, XEGImportContext{

	Workbench workbench;
	IOContext ctx;
	Node root;
		
	public XEGImportFilter(FilterItem item, FilterSource source)  {
		
		super(item, source);
		setFlavor(IOFlavor.valueOf(Node.class));	
		
	}
	
	public Object getObject() throws IOException {
		workbench = Workbench.get(source);
		workbench.beginStatus(this);
		workbench.setStatus(this, "Import XEG", -1);
		
		ctx = new IOContext();
		
		workbench.setProperty(IOContext.class.getName(), ctx);
		
		Reader reader = ((FileSource)source).getReader();
//		String url = toURL().getFile();
//		String fileName = url.substring(url. lastIndexOf('/')+1);
		
		root=null;
		
		// load the modelTypes of the current project
		HashMap<String, Type> modelTypes = getCompiledModelTypes();
		
		XEGImport importer = new XEGImport(reader, modelTypes, this, false);
		importer.doImport();
		
		workbench.clearStatusAndProgress(this);
		return root;
	}
	
	
	
	private HashMap<String, Type> getCompiledModelTypes(){
		Item classes_directory = getRegistry().getItem("/classes");
	    Item[] classes = Item.findAll(classes_directory, ItemCriterion.INSTANCE_OF, TypeItem.class, true);
	    HashMap<String, Type> modelTypes = new HashMap<String, Type>();
	    for (Item type: classes) {
	    	modelTypes.put(type.getName(), (Type) ((TypeItem)type).getObject());  
	    }
	    return modelTypes;
	}

	public void increaseProgress() {
		workbench.setStatus(this, "Import XEG");
		workbench.setIndeterminateProgress(this);
	}

	@Override
	public IOContext getIOContext() {
		return ctx;
	}

	@Override
	public Workbench getWorkbench() {
		return workbench;
	}

	@Override
	public Node getRoot() {
		return root;
	}

	@Override
	public void setRoot(Node r) {
		root = r;
	}
	
}
