package de.grogra.ext.exchangegraph;

import de.grogra.graph.impl.Node;
import de.grogra.pf.registry.RegistryContext;
import de.grogra.pf.ui.Workbench;

public interface XEGImportContext extends RegistryContext{

	public IOContext getIOContext();
	public Workbench getWorkbench();
	public Node getRoot();
	public void setRoot(Node r);
}
