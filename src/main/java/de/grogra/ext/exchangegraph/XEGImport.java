/*
 * Copyright (C) 2002 - 2007 Lehrstuhl Grafische Systeme, BTU Cottbus
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package de.grogra.ext.exchangegraph;

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Set;
import org.apache.xmlbeans.XmlError;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;

import de.grogra.ext.exchangegraph.helpnodes.XEGUnknown;
import de.grogra.ext.exchangegraph.xmlbeans.GraphDocument;
import de.grogra.ext.exchangegraph.xmlbeans.Property;
import de.grogra.grammar.RecognitionException;
import de.grogra.graph.Instantiator;
import de.grogra.graph.impl.Edge;
import de.grogra.graph.impl.Node;
import de.grogra.pf.io.ReaderSourceImpl;
import de.grogra.pf.registry.Item;
import de.grogra.pf.registry.Registry;
import de.grogra.pf.ui.UI;
import de.grogra.pf.ui.Workbench;
import de.grogra.reflect.Field;
import de.grogra.reflect.Type;
import de.grogra.rgg.Instance;
import de.grogra.rgg.RGGRoot;
import de.grogra.rgg.model.CompilationFilter;
import de.grogra.rgg.model.XLFilter;
import de.grogra.util.I18NBundle;
import de.grogra.util.MimeType;
import de.grogra.util.Utils;
import de.grogra.xl.util.BidirectionalHashMap;

public class XEGImport {

	private static final I18NBundle I18N = I18NBundle.getInstance (XEGImport.class);
	protected Reader graphReader;
	protected XEGImportContext source;
	protected HashMap<String, Type> modelTypes;
	protected boolean asProject;
	public boolean useInstantation;
	protected HashMap<String, de.grogra.ext.exchangegraph.xmlbeans.Type> toVisitXEGNodeType
			= new HashMap<String, de.grogra.ext.exchangegraph.xmlbeans.Type>();
	 
	/**
	 * Constructor called by GroIMP (Object->Insert File).
	 * 
	 * @throws IOException
	 */
	public XEGImport(Reader graphReader, HashMap<String, Type> modelTypes, XEGImportContext source,
			boolean asProject) {
		this.graphReader = graphReader;
		this.modelTypes = modelTypes;
		this.source = source;
		this.asProject= asProject;
		Item opt = Item.resolveItem (Workbench.current(), "/io/xeg_io");
		useInstantation = Utils.getBoolean (opt, "import_instantiation", false);
	}

	
	//TODO Find out how to add new edge 
//	@SuppressWarnings("unused")
//	private GraphDocument ajustEdgeFromRootForImport(GraphDocument graphDocument) throws Exception {
//		
//		de.grogra.ext.exchangegraph.xmlbeans.Graph graph = graphDocument.getGraph();
//		de.grogra.ext.exchangegraph.xmlbeans.Root root, groot;
//		if (graph.getRootList().size() !=1)
//			throw new Exception("The graph has too many roots!");
//		else{
//			 groot = graph.getRootList().get(0);
//			 root = groot;
//		}
//		List<de.grogra.ext.exchangegraph.xmlbeans.Edge> elist =  graph.getEdgeList();
//		List<Long> addEdgeIds = new ArrayList<Long>();
//		long changeTypeEdgeId = -1;
//		 for (de.grogra.ext.exchangegraph.xmlbeans.Edge e: elist){
//			 long destId = -1;
//			 if (e.getSrcId() == root.getRootId()){
//				 destId = e.getDestId();
//			 }
//			 List<de.grogra.ext.exchangegraph.xmlbeans.Edge> toEdges = new ArrayList<de.grogra.ext.exchangegraph.xmlbeans.Edge>();
//			 for (de.grogra.ext.exchangegraph.xmlbeans.Edge ee: elist){
//				 if (destId == ee.getDestId()){
//					 toEdges.add(ee);
//				 }
//			 }
//			 if (toEdges.size()==1){
//				 if (root == groot){
//				 changeTypeEdgeId = toEdges.get(0).getId();
//				 }else{
//					 final org.apache.xmlbeans.SchemaType stype = null;
//					 //de.grogra.ext.exchangegraph.xmlbeans.Edge newedge
//					 //graph.addNewEdge(newedge);
//				 }
//			 }
//		 }
//		return null;
//		
//	}
//	
	
	public void doImport() throws IOException {
		
		GraphDocument graphDocument = null;
		
		// parse document and test for errors
		try {
			XmlOptions opt = new XmlOptions();
			opt.setLoadLineNumbers();
			graphDocument = (GraphDocument) GraphDocument.Factory.parse(graphReader, opt);
		} catch (XmlException e) {
			throw new IOException("XEG parsing error: "
					+ e.getCause().getMessage());
		}
		
		ArrayList<XmlError> validationErrors = new ArrayList<XmlError>();
		XmlOptions validationOptions = new XmlOptions();
		validationOptions.setErrorListener(validationErrors);
		
		// copy the graph to the groimp graph
		copyGraph(graphDocument);
	}

	@SuppressWarnings("rawtypes")
	private void copyGraph(GraphDocument graphDocument) throws IOException {
		// obtain the root element of the xml file
		de.grogra.ext.exchangegraph.xmlbeans.Graph graph = graphDocument
				.getGraph();
		
		// create groimp node types for type declarations <-- XEG converted from MAppleT files doesn't contain any additional type, 
		// additional type means the extended type. When XEG is imported to a GroIMP graph, 
		// the type extensions need to be added to the GroIMP graph if there are modules defined in XL rules
 		
		List<String> xegtypeStrings = new ArrayList<String>();
		for (de.grogra.ext.exchangegraph.xmlbeans.Type type : graph
				.getTypeList()){
			xegtypeStrings.add(type.getName());
		}
		
		final HashMap<String, Type> additionalNodeTypes;
		
		additionalNodeTypes = createGroimpNodeTypes(graph);

		// read all node declarations and put them into a hashmap with id as key
		final BidirectionalHashMap<String, Node> nodeMap = source.getIOContext().getNodeMap();
		final BidirectionalHashMap<String, Edge> edgeMap = source.getIOContext().getEdgeMap();
		
		for (final de.grogra.ext.exchangegraph.xmlbeans.Node xmlNode : graph.getNodeList()) {

			Thread th = new Thread(new Thread() {
				@Override
				public void run() {
					String id = xmlNode.getId();
					String typeName = xmlNode.isSetType() ? xmlNode.getType() : "Node";
					Node groimpNode = null;
					try {
						groimpNode = createGroimpNode(typeName, xmlNode, additionalNodeTypes);
					} catch (IOException e) {
						// when a module exists in the compiled classes but its content is null (e.g. module A;)
						// return an empty node 
						groimpNode = new Node();
						groimpNode.setName(typeName);
						source.getRegistry().getLogger().log(source.getWorkbench().SOFT_GUI_INFO, 
								I18N.msg("Node: "+typeName+" couldn't be loaded. It was replaced by Node()")); 
					}
					nodeMap.put(id, groimpNode);
				}
			});
			th.start();
			try {
				th.join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
				
		// set the rgg id as the xeg root node by using xeg rootId
//		final long rootId = graph.getRootArray(0).getRootId();
		final String rootId = graph.getNodeArray(0).getId();
		if (source.getRoot()==null && nodeMap.containsKey(rootId) && nodeMap.get(rootId)!=null && nodeMap.get(rootId) instanceof Node) {
			source.setRoot(nodeMap.get(rootId));
		}
		nodeMap.put(rootId, source.getRoot());	
		
		// (plantId, edge from Graph root to plantId) map 
		final HashMap<String, de.grogra.ext.exchangegraph.xmlbeans.Edge> fromRootDstIdEdgeMap = source.getIOContext().getFromRootDstIdEdgeMap();
		
		// put nodes into GroIMP graph, namely connect GroIMP nodes to RggRoot by new created edges
		// firstly, connect Graph root to plant root of each scale
		final HashMap<String, de.grogra.ext.exchangegraph.xmlbeans.Edge> fromRootNonDecompositionNodeIdEdgeMap = new HashMap<String, de.grogra.ext.exchangegraph.xmlbeans.Edge>();
		final Set<String> intraScaleDestNodeIds = new HashSet<String>();
		
		for (final de.grogra.ext.exchangegraph.xmlbeans.Edge xmlEdge : graph.getEdgeList()) {
			
			Thread th = new Thread(new Thread(){
		        @Override
				public void run(){
		        	
					String srcId = xmlEdge.getSrcId();
					String xmlEdgeType = xmlEdge.getType().toLowerCase();
							
					if (srcId.equalsIgnoreCase( rootId )){
						String dstId = xmlEdge.getDestId();
						fromRootDstIdEdgeMap.put(dstId, xmlEdge);
						
						if (!xmlEdgeType.equals("decomposition")){
							fromRootNonDecompositionNodeIdEdgeMap.put(dstId, xmlEdge);
						}
					}
					
					//////////////////////
					if ("successor".equals(xmlEdge.getType().toLowerCase()) 
							|| "branch".equals(xmlEdge.getType().toLowerCase())
							){
						intraScaleDestNodeIds.add(xmlEdge.getDestId());
					}
					//////////////////////

					String dstId = xmlEdge.getDestId();
					Node srcNode = nodeMap.get(srcId);
					
					if (srcId != rootId) { 
						if (xmlEdgeType.equals("instantiation") && !useInstantation) {
						}
						else {
							Node dstNode = nodeMap.get(dstId);
							int edgeBit = getEdgeBit(xmlEdgeType);
							Edge e = srcNode.getOrCreateEdgeTo(dstNode);
							e.addEdgeBits(edgeBit, null);
							edgeMap.put(xmlEdge.getId(), e);
						}
					}	
		        } 
		    });
			
		    th.start();
		    try {
				th.join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
		Collection<String> nodeIds = nodeMap.getKeyMap();
		Collection<String> plantRootNodeIds = new HashSet<String>();
		
		for(String nid: nodeIds){
			plantRootNodeIds.add(nid);
		}
		
		plantRootNodeIds.remove(rootId);
		plantRootNodeIds.removeAll(intraScaleDestNodeIds);
		
		Node rootNd = nodeMap.get(rootId);

		if (fromRootNonDecompositionNodeIdEdgeMap.keySet().size() == 0) {

			if (plantRootNodeIds.size() == 1){
				int edgeBit = getEdgeBit("successor");
				Edge e = rootNd.getOrCreateEdgeTo(nodeMap.get(plantRootNodeIds.iterator().next()));
				e.addEdgeBits(edgeBit, null);
			}else{
				
				for(String xmlPlantRootNodeId : plantRootNodeIds) {
					int edgeBit = getEdgeBit("branch");
					Edge e = rootNd.getOrCreateEdgeTo(nodeMap.get(xmlPlantRootNodeId));
					e.addEdgeBits(edgeBit, null);
				}
			}
		}else{
			// if root of the xeg is not root of project graph
			for (Iterator<de.grogra.ext.exchangegraph.xmlbeans.Edge> iterator = fromRootNonDecompositionNodeIdEdgeMap.values().iterator(); iterator.hasNext();){
				de.grogra.ext.exchangegraph.xmlbeans.Edge fromRootNonDecompositionEdge = iterator.next();
				String xmlEdgeType = fromRootNonDecompositionEdge.getType().toLowerCase();
				
				if (xmlEdgeType.equals("instantiation") && !useInstantation) {
				}
				else {
					int edgeBit = getEdgeBit(xmlEdgeType);
					Node dstNode = nodeMap.get(fromRootNonDecompositionEdge.getDestId());
					Edge e = rootNd.getOrCreateEdgeTo(dstNode);
					e.addEdgeBits(edgeBit, null);
				}
			}
		}

	}


	/**
	 * Add the type graph to adapted groimp graph, so when exporting (node/edge map will be used), 
	 * no need to remove it from XEG graph. Also, add typegraph to XEG graph need to add new xmlEdge, 
	 * and srcid and dstid need to be set.
	 * 
	 * @param graph
	 * @param scaleTypesMap
	 * @throws IOException
	 */
	@SuppressWarnings("unused")
	private void addTypegraph(de.grogra.ext.exchangegraph.xmlbeans.Graph graph,
			LinkedHashMap<String, Set<String>> scaleTypesMap) throws IOException {

//		long xmlEdgeIdMax = 0;
//		for (de.grogra.ext.exchangegraph.xmlbeans.Edge xmlEdge : graph.getEdgeList()) {
//			if (xmlEdgeIdMax < xmlEdge.getId())
//				xmlEdgeIdMax = xmlEdge.getId();
//		}

		Node preType = createGroimpNonGeoNode("TypeRoot", "TypeRoot");
		Node preScale = createGroimpNonGeoNode("SRoot", "SRoot");

		connectNodes(source.getRoot(), preType, "decomposition");
		connectNodes(source.getRoot(), preScale, "decomposition");

		for (String scaleName : scaleTypesMap.keySet()) {

			// add scale node and decomposition edge
			Node scaleNode = createGroimpNonGeoNode("ScaleClass", scaleName);
			connectNodes(preScale, scaleNode, "decomposition");

			// add type node and decomposition edge
			// Set<Node> typeNodeSet = new HashSet<Node>();
			ArrayList<Node> typeNodeList = new ArrayList<Node>();
			for (String typeName : scaleTypesMap.get(scaleName)) {
				Node typeNode = createGroimpNonGeoNode(typeName, typeName);
				typeNodeList.add(typeNode);
				connectNodes(preType, typeNode, "decomposition");
			}

			// add bidirectional successor and branch edges between type node
			for (Node typeNode : typeNodeList) {

				for (Node restNode : typeNodeList) {
					// no duplicated type, so index is unique
					if (typeNodeList.indexOf(restNode) == typeNodeList.indexOf(typeNode))
						continue;
					connectNodes(typeNode, restNode, "successor");
					connectNodes(typeNode, restNode, "branch");
				}
			}
		}
	}
	
	
	/**
	 * Creates GroIMP nodes for ScaleClass node in type graph.
	 * 
	 * @param typeName
	 * @return
	 */
	private Node createGroimpNonGeoNode(String typeName, String scaleName) throws IOException {

		Node node = null;
		// create instance
		try {
			if (IOContext.importNodeTypes.containsKey(typeName)) {
				node = (Node) Class.forName(IOContext.importNodeTypes.get(typeName)).getDeclaredConstructor().newInstance();
			}
		} catch (Exception e) {
			System.out.println(typeName);
			e.printStackTrace();
		}
		// set name
		node.setName("Scale" + scaleName);

		return node;
	}
	
	
	private void connectNodes(Node srcNode, Node dstNode, 
			String edgeType){
		if (edgeType.equals("instantiation") && !useInstantation) {
		}
		else {
			int edgeBit = getEdgeBit(edgeType);
			Edge e = srcNode.getOrCreateEdgeTo(dstNode);
			e.addEdgeBits(edgeBit, null);
		}
	}

	
	/**
	 * Check if the given Type matches with one of the required xeg import type. 
	 * e.g. The xeg file define a custom type A that extends Sphere and have one 
	 * property: len- a float. The current model does have a module A, but it do not 
	 * have a property len. Then, the xegType is added to the list as a new type.
	 * 
	 * @param var1
	 * @param var2
	 * @return
	 */
	protected boolean typeMatchList(Type type, List<de.grogra.ext.exchangegraph.xmlbeans.Type> list) {
		for (de.grogra.ext.exchangegraph.xmlbeans.Type xegtype : list) {
			if (typeMatchType(type, xegtype)) {
				return true;
			}
		}
		return false;
	}
	
	
	protected boolean typeMatchType(Type type, de.grogra.ext.exchangegraph.xmlbeans.Type xegtype) {
		toVisitXEGNodeType.remove(xegtype.getName());
		// first get one that matches the name
		if (!xegtype.getName().equals(type.getSimpleName())  ) {
			return false;
		}
		
		// 2nd match the extension (super?) - the xeg super type should only be tested if they
		// are custom type (defined in xeg) and only tested once
		String supername = xegtype.getExtends().getName();
		if (supername != null) {
			de.grogra.ext.exchangegraph.xmlbeans.Type xegsupertype = toVisitXEGNodeType.get(supername);
			if (xegsupertype != null) {
				if (!typeMatchType(type.getSupertype(), xegsupertype)) {
					return false;
				}
			} // else it is not a custom node - checks the IOContext nodes 
			else if (!source.getIOContext().importNodeTypes.get(supername) // could use reflexion & get the class but well
					.equals(type.getSupertype().getName()) ) {
					return false;
			}
		}

		// then, check the properties
		int nbField = type.getDeclaredFieldCount();
		if ( nbField-1 != xegtype.getPropertyList().size() ) {
			return false;
		}
		for ( Property property : xegtype.getPropertyList() ) {
			// each one (except $Field) have to match to one
			boolean matched = false;
			for (int i = 0; i < nbField; i++) {
				if (propertyMatchField(property, type.getDeclaredField(i))) {
					matched=true;
				}
			}
			if (!matched) {
				return false;
			}
		}
		return true;
	}
	
	
	protected boolean propertyMatchField(Property p , Field field) {
			if (field.getName().equals("$Field")) {
			}
			else if ( (field.getName().equals(p.getName())) 
					&& (field.getType().getName().equals(p.getType())) ) {
				return true;
			}
		
		return false;
		
	}
	
	@SuppressWarnings({ "rawtypes" })
	protected HashMap<String, Type> createGroimpNodeTypes(
			de.grogra.ext.exchangegraph.xmlbeans.Graph graph) {
		HashMap<String, Type> additionalNodeTypes = new HashMap<String, Type>();
		String simplifiedName;
		
		for ( de.grogra.ext.exchangegraph.xmlbeans.Type t : graph.getTypeList() ) {
			toVisitXEGNodeType.put(t.getName(), t);
		}
		
		if (modelTypes!=null) {
			for (Type t : this.modelTypes.values()) {
				if (typeMatchList(t, graph.getTypeList())) {
					simplifiedName = t.getName();
					simplifiedName = simplifiedName.substring(simplifiedName.indexOf(".")+1);
					additionalNodeTypes.put(simplifiedName, t);
				}
			}
		}
//		return additionalNodeTypes;
		// TODO: Make sure custom imported nodes are not REINSTANTAITED
		
		Type<?>[] compiledTypes = null;
		
		StringBuffer newTypesString = new StringBuffer();
		newTypesString.append("import de.grogra.imp3d.objects.*;");
		for (de.grogra.ext.exchangegraph.xmlbeans.Type type : graph
				.getTypeList()) {
			if (IOContext.importNodeTypes.containsKey(type.getName()))
				continue;

			newTypesString.append("public class ");
			newTypesString.append(type.getName());
			newTypesString.append(" extends ");
			String extendType = type.getExtends().getName();
			newTypesString.append(IOContext.importNodeTypes
					.containsKey(extendType) ? IOContext.importNodeTypes
					.get(extendType) : extendType);
			newTypesString.append("{");
			for (Property property : type.getPropertyList()) {
				newTypesString.append(property.getType() + " "
						+ property.getName() + ";");
			}
			newTypesString.append("};");
			
			
		}
		// compile xl string to get types
		try {
			XLFilter xlFilter = new XLFilter (null, new ReaderSourceImpl (new StringReader(newTypesString
					.toString()), "XEGNodeTypes", MimeType.TEXT_PLAIN, Registry.current (), null));
			
			CompilationFilter compilationFilter = new CompilationFilter (null, xlFilter);
			
			compiledTypes = compilationFilter.compile(null, null);	
		} catch (IOException e) {
			if (e.getCause() instanceof RecognitionException)
				System.err.println(((RecognitionException) e.getCause())
						.getDetailedMessage(false));
		} catch (Exception e) {
			System.err.println(e.getCause());
		}		


		for (Type compiledType : compiledTypes) {
			// do not use getSimpleName, instead use getName without modelName
			String typeName = compiledType.getName();
			typeName = typeName.substring(typeName.indexOf(".")+1);
			if (!additionalNodeTypes.containsKey(typeName))
					additionalNodeTypes.put(typeName, compiledType);
		}
		return additionalNodeTypes;
	}
	
	
	@SuppressWarnings({ "rawtypes", "unused" })
	private HashMap<String, HashMap<Type, Object>> getTypeFields(de.grogra.reflect.Type t){
		
		int modulefieldCount = t.getDeclaredFieldCount();

		HashMap<String, HashMap<Type, Object>> fntv = new HashMap<String, HashMap<Type, Object>>();
		
		for (int i = 0; i < modulefieldCount; i++) {
			Type fieldType = t.getDeclaredField(i).getType();
			if (!fieldType.getSimpleName().equals("NType")) {
				String fieldName = t.getDeclaredField(i).getSimpleName();
				Object fieldValue = t.getDefaultElementValue(fieldName);
				HashMap<Type, Object> fTypeValue = new HashMap<Type, Object>();
				fTypeValue.put(fieldType, fieldValue);
				fntv.put(fieldName, fTypeValue);
			}
		}
		
		return fntv;
	}
	
	
	/**
	 * Creates GroIMP nodes for specified type.
	 * 
	 * @param typeName
	 * @return
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private Node createGroimpNode(String typeName,
			de.grogra.ext.exchangegraph.xmlbeans.Node xmlNode,
			HashMap<String, Type> additionalNodeTypes) throws IOException, NullPointerException {

		Node node = null;

		// create instance
		
		try {
			if (IOContext.importNodeTypes.containsKey(typeName)) {
				node = (Node) Class.forName(
						IOContext.importNodeTypes.get(typeName)).getDeclaredConstructor().newInstance();
			} else {
				node = (Node) additionalNodeTypes.get(typeName).newInstance();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		// set properties
		List<Property> properties = xmlNode.getPropertyList();
		List<Property> handledProperties = new ArrayList<Property>(properties
				.size());
		Class nodeClass = node.getClass();

		try {
			// loop over hierarchy to set standard properties
			do {
				if (IOContext.xegNodeTypes.containsKey(nodeClass)) {
					Class xegClass = IOContext.xegNodeTypes.get(nodeClass);
					Method m = xegClass.getMethod("handleImportProperties",
							Node.class, List.class, List.class);
					m.invoke(null, node, properties, handledProperties);

					// remove set attributes
					for (Property p : handledProperties)
						properties.remove(p);
					handledProperties.clear();
				}
				// continue with superclass
				nodeClass = nodeClass.getSuperclass();
			} while (nodeClass != Object.class);

			// now set unknown properties (the rest in the properties list)
			XEGUnknown.handleImportProperties(node, properties);	
			
		} catch (IllegalArgumentException | IllegalAccessException | NoSuchMethodException | 
				SecurityException | InvocationTargetException e) {
			// find root node of scene
			Node rootNode = UI.getRootOfProjectGraph(source.getWorkbench());
			if (rootNode != null){
			// then find the rggRoot
				if (rootNode.getFirstEdge() == null){
					RGGRoot rggRoot = new RGGRoot();
					rootNode.addEdgeBitsTo(rggRoot, de.grogra.graph.Graph.BRANCH_EDGE, null);
				}
			}else{
				System.out.println("rootNode is null!!!");;
			}
			throw new IOException("Couldn't create GroIMP node: " + e.getMessage());
		}

		// set name
		if (xmlNode.isSetName()	&& (!xmlNode.getName().trim().equals("")))
			node.setName(xmlNode.getName());

		return node;
	}

	/**
	 * Returns the GroIMP specific edge bits for a given xml edge type.
	 * 
	 * @param type
	 * @return
	 */
	protected int getEdgeBit(String type) {
		BidirectionalHashMap<Integer, String> edgeTypes = source.getIOContext().getEdgeTypes();

		if ("successor".equals(type))
			return de.grogra.graph.Graph.SUCCESSOR_EDGE;
		if ("branch".equals(type))
			return de.grogra.graph.Graph.BRANCH_EDGE;
		if ("decomposition".equals(type))
			return de.grogra.graph.Graph.REFINEMENT_EDGE;
		if ("instantiation".equals(type))
			return de.grogra.graph.Graph.INSTANTIATION_EDGE+de.grogra.graph.Graph.BRANCH_EDGE;
		if (edgeTypes.containsValue(type)) {
			return edgeTypes.getKey(type);
		} else {
			int minEdge = edgeTypes.size();
			edgeTypes.put(minEdge, type);
			return de.grogra.graph.Graph.MIN_UNUSED_EDGE << minEdge;
		}
	}

}
