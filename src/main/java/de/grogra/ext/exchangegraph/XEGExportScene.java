package de.grogra.ext.exchangegraph;

import java.io.File;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

import org.apache.xmlbeans.XmlOptions;

import de.grogra.ext.exchangegraph.export.BaseExportEdge;
import de.grogra.ext.exchangegraph.export.BaseExportNode;
import de.grogra.ext.exchangegraph.export.InstantiatedExportNode;
import de.grogra.ext.exchangegraph.xmlbeans.GraphDocument;
import de.grogra.graph.ArrayPath;
import de.grogra.graph.Path;
import de.grogra.graph.impl.Edge;
import de.grogra.graph.impl.Node;
import de.grogra.imp3d.View3D;
import de.grogra.imp3d.io.SceneGraphExport;
import de.grogra.imp3d.objects.Attributes;
import de.grogra.imp3d.objects.SceneTree;
import de.grogra.imp3d.objects.SceneTreeWithShader;
import de.grogra.imp3d.objects.SceneTree.InnerNode;
import de.grogra.math.TMatrix4d;
import de.grogra.pf.io.FileWriterSource;
import de.grogra.pf.io.FilterItem;
import de.grogra.pf.io.FilterSource;
import de.grogra.pf.ui.Workbench;
import de.grogra.rgg.RGGRoot;
import de.grogra.util.I18NBundle;
import de.grogra.xl.util.BidirectionalHashMap;

public class XEGExportScene extends SceneGraphExport implements FileWriterSource {

	private static final I18NBundle I18N = I18NBundle.getInstance (XEGExportScene.class);
	public IOContext ctx;
	Workbench workbench;
	public de.grogra.ext.exchangegraph.xmlbeans.Graph graph = null;
	public final Set<String> customTypes = new HashSet<String>();
	public BidirectionalHashMap<Integer, String> customEdgeTypes ;
	AtomicInteger idNode = new AtomicInteger();
	TMatrix4d stack = new TMatrix4d();
	
	public int getNewNodeID() {
		if (idNode.get() == Integer.MAX_VALUE) {
			idNode.set(0);
		}
		return idNode.incrementAndGet();
	}
	
	public XEGExportScene(FilterItem item, FilterSource source) {
		super(item, source);
		setFlavor (item.getOutputFlavor ());
		this.workbench = Workbench.current();
	}

	@Override
	public void write(File out) throws IOException {
		workbench.beginStatus(this);
		workbench.setStatus(this, "Export XEG", -1);
		
//		Node rootNode = UI.getRootOfProjectGraph(workbench);
		IOContext ctx = (IOContext)Workbench.current().getProperty(IOContext.class.getName());
		if (ctx == null ){
			ctx= new IOContext();
		}
		this.ctx=ctx;
		customEdgeTypes= ctx.getEdgeTypes();
				
		GraphDocument xegDocument = GraphDocument.Factory.newInstance();
		graph = xegDocument.addNewGraph();
		
		// if the graph have a rggroot. Write it down as root node
		Node rroot = RGGRoot.getRoot( workbench.getRegistry().getProjectGraph());
		if (rroot != null) {
			graph.addNewRoot().setRootId(String.valueOf( rroot.getId() ));
		}
		
		write();
		
		XmlOptions xmlOptions = new XmlOptions();
		xmlOptions.setSavePrettyPrint();
		xmlOptions.setSavePrettyPrintIndent(2);
		
		// save file
		xegDocument.save(out, xmlOptions);
		
		workbench.clearStatusAndProgress(this);
	}

	@Override
	protected SceneTree createSceneTree(View3D scene) {
		SceneTree t = new SceneTreeWithShader(scene) {
			@Override
			protected boolean acceptLeaf(Object object, boolean asNode) {
				return getExportFor(object, asNode) != null;
			}
			@Override
			protected Leaf createLeaf(Object object, boolean asNode, long id) {
				Leaf l = new Leaf(object, asNode, id);
				init(l);
				return l;
			}
		};
		t.createTree(false, ( (Boolean) workbench.getProperty(Workbench.EXPORT_VISIBLE_LAYER) == null)? false: 
			(Boolean) workbench.getProperty(Workbench.EXPORT_VISIBLE_LAYER));
		return t;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public NodeExport getExportFor (Object object, boolean asNode)
	{
		if (object instanceof Node && ((Node)object).getIgnored()) {
			return null; // remove ignored nodes
		}
		if (!asNode && (
				( ((Edge)object).getTarget()!=null && ((Edge)object).getTarget().getIgnored() ) || 
				( ((Edge)object).getSource()!=null && ((Edge)object).getSource().getIgnored() )
				)) { // remove edges that originates from/ target ignored objects 
			return null;
		}
		if (!asNode) {
			return new BaseExportEdge();
		}
		Path p = getGraphState().getInstancingPath();
		if (p != null) {
			Object s = getGraphState ().getObjectDefault // during instantiation only export shaped objects
					(object, asNode, Attributes.SHAPE, null);
			if (s!=null) {
				return new InstantiatedExportNode((Node)p.getObject (0), (ArrayPath)p);
			}
			return null;
		}
		return new BaseExportNode();
	}

	@Override
	protected void beginGroup(InnerNode group) throws IOException {
		// nodes don't get grouped in xeg (todo?)	
	}

	@Override
	protected void endGroup(InnerNode group) throws IOException {
		// nodes don't get grouped in xeg (todo?)
	}
	
}
