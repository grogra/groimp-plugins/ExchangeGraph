package de.grogra.ext.exchangegraph.export;

import java.io.IOException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Stack;

import javax.vecmath.Matrix4d;

import de.grogra.ext.exchangegraph.IOContext;
import de.grogra.ext.exchangegraph.XEGExportScene;
import de.grogra.ext.exchangegraph.helpnodes.XEGNode;
import de.grogra.ext.exchangegraph.helpnodes.XEGUnknown;
import de.grogra.ext.exchangegraph.xmlbeans.ExtendsType;
import de.grogra.ext.exchangegraph.xmlbeans.Property;
import de.grogra.graph.ArrayPath;
import de.grogra.graph.GraphState;
import de.grogra.graph.Instantiator;
import de.grogra.graph.Path;
import de.grogra.graph.impl.Node;
import de.grogra.graph.impl.Node.NType;
import de.grogra.imp3d.Visitor3D;
import de.grogra.imp3d.io.SceneGraphExport;
import de.grogra.imp3d.objects.Attributes;
import de.grogra.imp3d.objects.SceneTree.InnerNode;
import de.grogra.imp3d.objects.SceneTree.Leaf;
import de.grogra.math.TMatrix4d;
import de.grogra.math.Transform3D;
import de.grogra.persistence.ManageableType;
import de.grogra.reflect.Reflection;

public class InstantiatedExportNode implements SceneGraphExport.NodeExport{

	Node parent;
	ArrayPath path;
	
	public InstantiatedExportNode(Node parent, ArrayPath p) {
		this.parent = parent;
		this.path = p;
	}
	
	@Override
	public void export(Leaf node, InnerNode transform, SceneGraphExport sge) throws IOException {
		XEGExportScene export = (XEGExportScene) sge;
		IOContext ctx = export.ctx;
		
		if (!(node.object instanceof Node)){return;}
		
		Node n = (Node)node.object;
		// Make sure the type of the node is either a base groimp type, or already added
		String type = writeType(export.customTypes, n, export.graph);
		
		// write node to xml document
		de.grogra.ext.exchangegraph.xmlbeans.Node xmlNode = export.graph
				.addNewNode();
		
		int id = export.getNewNodeID();
		
		xmlNode.setId("i" + String.valueOf( id ));
		xmlNode.setName(n.getName());
		xmlNode.setType(type);
		
		// write standard node properties
		List<Class> unknownTypes = new ArrayList<Class>(); 
		boolean unknownType = true;
		Class<?> nodeClass = n.getClass();
		List<Class> handledClasses = new ArrayList<Class>();
		try {
			// loop over hierarchy to set standard properties
			do {
				if (IOContext.xegNodeTypes.containsKey(nodeClass)) {
					Class<?> xegClass = IOContext.xegNodeTypes.get(nodeClass);
					if (!handledClasses.contains(xegClass)) {
						Method m = xegClass.getMethod("handleExportProperties", Node.class, de.grogra.ext.exchangegraph.xmlbeans.Node.class);
						m.invoke(null, n, xmlNode);
						unknownType = false;
						handledClasses.add(xegClass);
					}
				}
				else {
					if (unknownType)
						unknownTypes.add(nodeClass);
				}
				// continue with superclass
				nodeClass = nodeClass.getSuperclass();
			} while (nodeClass != Object.class);
		} catch (Exception e) {e.printStackTrace();}

		// write unknown property values of the node
		XEGUnknown.handleExportProperties(n, xmlNode, unknownTypes);
		
		// add the global transform of the instantiated node (global between the 
		// root of the instantiation and the "shape" object
		Matrix4d completeTransform = new Matrix4d();
		Matrix4d parentTransform = new Matrix4d();
		ArrayPath p = new ArrayPath(path);
		
		TransformVisitor3D visitor = new TransformVisitor3D();
		visitor.init(GraphState.current (p.getGraph()));
		completeTransform = visitor.getGlobalTransformation(p);
		parentTransform = visitor.getGlobalTransformation(parent);
		// the instantiated edges are added to the parent 
		// which make the parent transform to be displayed - 
		// While the instantiation appear BEFORE the parent transform - so remove it
//		parentTransform.invert();
//		completeTransform.mul(parentTransform);
		
		Property xmlProperty = xmlNode.addNewProperty();
		xmlProperty.setName("transform");
		xmlProperty.setMatrix(XEGNode.getElementsOfMatrix(completeTransform));
		

		
		
		// create an "instantiation edge" to link the node to the parent
		de.grogra.ext.exchangegraph.xmlbeans.Edge xmlEdge = export.graph
				.addNewEdge();
		xmlEdge.setId("-i1");
		xmlEdge.setSrcId(String.valueOf( parent.getId() ));
		xmlEdge.setDestId("i"+id);
		xmlEdge.setType("instantiation");
	}
	
	
	private String writeType(Set<String> types, Node groimpNode,
			de.grogra.ext.exchangegraph.xmlbeans.Graph graph) {

		NType nodeType = groimpNode.getNType();
		Class<?> nodeClass = groimpNode.getClass();

		String type = getTypeForNodeClass(nodeClass);
		String resultType = new String(type);

		// write type to xml document if not already exists
		while (!types.contains(type)) {
			
			// do not add standard node types
			if (IOContext.importNodeTypes.containsKey(type))
				break;

			// add type to xml file
			types.add(type);
			de.grogra.ext.exchangegraph.xmlbeans.Type xmlType = graph
					.addNewType();
			xmlType.setName(type);

			// set properties to type
			int fieldCount = nodeType.getManagedFieldCount();
			for (int i = 0; i < fieldCount; i++) {
				ManageableType.Field mf = nodeType.getManagedField(i);
				if ((mf.getDeclaringType() == nodeType)
						&& (Reflection.isPrimitiveOrString(mf.getType()))) {
					Property xmlProperty = xmlType.addNewProperty();
					xmlProperty.setName(mf.getSimpleName());
					xmlProperty.setType(mf.getType().getSimpleName());
				}
			}

			// find out if super class needs to be written to xml file
			if (!IOContext.exportNodeTypes.containsKey(nodeClass.getName())) {
				ExtendsType extend = xmlType.addNewExtends();
				nodeClass = nodeClass.getSuperclass();
				nodeType = (NType) nodeType.getManageableSupertype();
				type = getTypeForNodeClass(nodeClass);
				extend.setName(type);
			}
		}
		return resultType;
	}
	
	private String getTypeForNodeClass(Class<?> nodeClass) {
		String className = nodeClass.getName();
		if (className.indexOf("$") != -1)
			className = className.substring(className.indexOf("$") + 1);
		String type = IOContext.exportNodeTypes.get(nodeClass.getName());
		if (type == null) {
			type = nodeClass.getSimpleName();
		}
		return type;
	}

	
	class TransformVisitor3D extends Visitor3D
	{
		protected void init(GraphState gs)
		{
			Matrix4d m = new Matrix4d ();
			m.setIdentity ();
			init (gs, gs.getGraph ().getTreePattern (), m);
		}
		
		@Override
		protected void visitEnterImpl(Object object, boolean asNode,
				Path path) {
		}

		@Override
		protected void visitLeaveImpl(Object object, boolean asNode,
				Path path) {
		}

		
		public Matrix4d getGlobalTransformation(ArrayPath p)
		{
			Matrix4d mtrx = new Matrix4d();
						
			// We need the transformation up to the last object of the path 
			// (not including it). So we need to remove it and its edge first
			if (p.getNodeAndEdgeCount()<=3) {
				mtrx = getGlobalTransformation(parent);
				mtrx.invert();
				return mtrx;
			}
			p.popNode();
			p.popEdgeSet();
			
			Stack<Node> stack = new Stack<Node>();
			Node tmp_node;
			boolean asNode=true;
			int count = p.getNodeAndEdgeCount();
			for (int i=0; i<count-1; i++) {
				if (asNode) {
					tmp_node = (Node) p.popNode();
					stack.push(tmp_node);
					asNode=false;
				}else {
					p.popEdgeSet();
					asNode=true;
				}
			}
			// We compute the tranformation for an instantiation object, which are 
			// displayed BEFORE the object itself (so, if the object extend a movement
			// we need to NOT take it into account
			tmp_node = (Node) p.popNode();
			stack.push(tmp_node);
									
			Node root = stack.pop();
								
			return getGlobalTransformationFirst(root,stack,p);
		}
		
		public Matrix4d getGlobalTransformation(Node node)
		{
			Matrix4d mtrx = new Matrix4d();
			mtrx.setIdentity();
			
			Stack<Node> stack = new Stack<Node>();
			ArrayPath p = new ArrayPath(path.getGraph());
								
			return getGlobalTransformation(node, stack, p);
		}
		
		private Matrix4d getGlobalTransformation(Node v, Stack<Node> stack, ArrayPath path) {
			Matrix4d mtrx;
			
			path.pushNode (v, v.getId() >= 0 ? v.getId() : v.hashCode ());
			visitEnter(path, true);
			
//			Instantiator i;
//			if ((i = v.getInstantiator ()) != null)
//			{
//				boolean b = true;
//				state.beginInstancing (v,
//						path.getObjectId (-1));
//				b = i.instantiate (path, this);
//			}
			
			if( !stack.empty() )
			{
				Node child = stack.pop();
				
				path.pushEdgeSet (v.getEdgeTo(child), -1, false);
						
				mtrx = getGlobalTransformation( child, stack, path);

				path.popEdgeSet ();
			}
			else
			{
				mtrx = (Matrix4d) getCurrentTransformation().clone();
			}
			
//			if ((i = v.getInstantiator ()) != null)
//			{
//				state.endInstancing ();
//			}
			
			visitLeave(v, path, true);
			path.popNode ();
			
			return mtrx;
		}
		
		private Matrix4d getGlobalTransformationFirst(Node v, Stack<Node> stack, ArrayPath path) {
			Matrix4d mtrx;
			
			path.pushNode (v, v.getId() >= 0 ? v.getId() : v.hashCode ());
			visitEnter(path, true);
			
			// the first node (the instantiator node ) need to be reversed 
			mtrx = (Matrix4d) getCurrentTransformation();
			mtrx.invert();

			
			if( !stack.empty() )
			{
				Node child = stack.pop();
				
				path.pushEdgeSet (v.getEdgeTo(child), -1, false);
						
				mtrx = getGlobalTransformation( child, stack, path);

				path.popEdgeSet ();
			}
			else
			{
				mtrx = (Matrix4d) getCurrentTransformation().clone();
			}
			
			visitLeave(v, path, true);
			path.popNode ();
			
			return mtrx;
		}
	};
	
	
}
