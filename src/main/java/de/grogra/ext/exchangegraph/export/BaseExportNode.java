package de.grogra.ext.exchangegraph.export;

import java.io.IOException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import de.grogra.ext.exchangegraph.IOContext;
import de.grogra.ext.exchangegraph.XEGExportScene;
import de.grogra.ext.exchangegraph.helpnodes.XEGUnknown;
import de.grogra.ext.exchangegraph.xmlbeans.ExtendsType;
import de.grogra.ext.exchangegraph.xmlbeans.Property;
import de.grogra.graph.impl.Node;
import de.grogra.graph.impl.Node.NType;
import de.grogra.imp3d.io.SceneGraphExport;
import de.grogra.imp3d.objects.SceneTree.InnerNode;
import de.grogra.imp3d.objects.SceneTree.Leaf;
import de.grogra.persistence.ManageableType;
import de.grogra.reflect.Reflection;

public class BaseExportNode implements SceneGraphExport.NodeExport{

	@Override
	public void export(Leaf node, InnerNode transform, SceneGraphExport sge) throws IOException {
		XEGExportScene export = (XEGExportScene) sge;
		IOContext ctx = export.ctx;
		
		if (!(node.object instanceof Node)){return;}
		
		Node n = (Node)node.object;
		// Make sure the type of the node is either a base groimp type, or already added
		String type = writeType(export.customTypes, n, export.graph);
		
		// write node to xml document
		de.grogra.ext.exchangegraph.xmlbeans.Node xmlNode = export.graph
				.addNewNode();
		xmlNode.setId(String.valueOf( n.getId()) );
		xmlNode.setName(n.getName());
		xmlNode.setType(type);
		
		// write standard node properties
		List<Class> unknownTypes = new ArrayList<Class>(); 
		boolean unknownType = true;
		Class<?> nodeClass = n.getClass();
		List<Class> handledClasses = new ArrayList<Class>();
		try {
			// loop over hierarchy to set standard properties
			do {
				if (IOContext.xegNodeTypes.containsKey(nodeClass)) {
					Class<?> xegClass = IOContext.xegNodeTypes.get(nodeClass);
					if (!handledClasses.contains(xegClass)) {
						Method m = xegClass.getMethod("handleExportProperties", Node.class, de.grogra.ext.exchangegraph.xmlbeans.Node.class);
						m.invoke(null, n, xmlNode);
						unknownType = false;
						handledClasses.add(xegClass);
					}
				}
				else {
					if (unknownType)
						unknownTypes.add(nodeClass);
				}
				// continue with superclass
				nodeClass = nodeClass.getSuperclass();
			} while (nodeClass != Object.class);
		} catch (Exception e) {e.printStackTrace();}

		// write unknown property values of the node
		XEGUnknown.handleExportProperties(n, xmlNode, unknownTypes);

		
	}
	
	
	private String writeType(Set<String> types, Node groimpNode,
			de.grogra.ext.exchangegraph.xmlbeans.Graph graph) {

		NType nodeType = groimpNode.getNType();
		Class<?> nodeClass = groimpNode.getClass();

		String type = getTypeForNodeClass(nodeClass);
		String resultType = new String(type);

		// write type to xml document if not already exists
		while (!types.contains(type)) {
			
			// do not add standard node types
			if (IOContext.importNodeTypes.containsKey(type))
				break;

			// add type to xml file
			types.add(type);
			de.grogra.ext.exchangegraph.xmlbeans.Type xmlType = graph
					.addNewType();
			xmlType.setName(type);

			// set properties to type
			int fieldCount = nodeType.getManagedFieldCount();
			for (int i = 0; i < fieldCount; i++) {
				ManageableType.Field mf = nodeType.getManagedField(i);
				if ((mf.getDeclaringType() == nodeType)
						&& (Reflection.isPrimitiveOrString(mf.getType()))) {
					Property xmlProperty = xmlType.addNewProperty();
					xmlProperty.setName(mf.getSimpleName());
					xmlProperty.setType(mf.getType().getSimpleName());
				}
			}

			// find out if super class needs to be written to xml file
			if (!IOContext.exportNodeTypes.containsKey(nodeClass.getName())) {
				ExtendsType extend = xmlType.addNewExtends();
				nodeClass = nodeClass.getSuperclass();
				nodeType = (NType) nodeType.getManageableSupertype();
				type = getTypeForNodeClass(nodeClass);
				extend.setName(type);
			}
		}
		return resultType;
	}
	
	private String getTypeForNodeClass(Class<?> nodeClass) {
		String className = nodeClass.getName();
		if (className.indexOf("$") != -1)
			className = className.substring(className.indexOf("$") + 1);
		String type = IOContext.exportNodeTypes.get(nodeClass.getName());
		if (type == null) {
			type = nodeClass.getSimpleName();
		}
		return type;
	}
}
