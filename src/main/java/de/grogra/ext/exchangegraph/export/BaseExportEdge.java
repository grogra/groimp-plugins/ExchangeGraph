package de.grogra.ext.exchangegraph.export;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import de.grogra.ext.exchangegraph.IOContext;
import de.grogra.ext.exchangegraph.XEGExportScene;
import de.grogra.graph.Graph;
import de.grogra.graph.impl.Edge;
import de.grogra.graph.impl.Node;
import de.grogra.imp3d.io.SceneGraphExport;
import de.grogra.imp3d.objects.SceneTree.InnerNode;
import de.grogra.imp3d.objects.SceneTree.Leaf;
import de.grogra.xl.util.BidirectionalHashMap;

public class BaseExportEdge implements SceneGraphExport.NodeExport{

	@Override
	public void export(Leaf node, InnerNode transform, SceneGraphExport sge) throws IOException {
		XEGExportScene export = (XEGExportScene) sge;
		IOContext ctx = export.ctx;
		
		if (!(node.object instanceof Edge)) {return;}
		
		Edge e = (Edge)node.object;

		// write edges to xml document			
		Node targetNode = e.getTarget();
		Node srcNode = e.getSource();
		
		String dstId = String.valueOf( targetNode.getId() );

		// write only incoming edges to avoid duplicates
		int edgeBits = e.getEdgeBits();
		
//		if (srcNode.equals(root)){
//			
//			boolean singleToTargetEdge = true;
//			
//			for (Edge groimpEdgee = groimpNode.getFirstEdge(); groimpEdgee != null; groimpEdgee = groimpEdgee
//					.getNext(groimpNode)) {
//				
//				if (groimpEdgee.getTarget().equals(targetNode) && !groimpEdgee.getSource().equals(root)){
//					singleToTargetEdge = false;
//					break;
//				}	
//			}
//			
//			if (singleToTargetEdge){
//				
//				boolean isMultiScale = false;
//				
//				for (Edge groimpEdgee = groimpNode.getFirstEdge(); groimpEdgee != null; groimpEdgee = groimpEdgee
//						.getNext(groimpNode)) {
//					if (groimpEdgee.getSource().equals(targetNode) &&
//						(groimpEdgee.getEdgeBits() & Graph.REFINEMENT_EDGE) == Graph.REFINEMENT_EDGE){
//						isMultiScale = true;
//						break;
//					}
//				}
//				
//				if(isMultiScale){
//					addXmlEdge(getEdgeBit("decomposition"), rootId, dstId, edgeId, edgeTypes, graph);
//					
//				}else{
//					Set<Node> subNodes = new HashSet<Node>();
//					visitNodes(subNodes, targetNode);
//					for(Node subNode: subNodes){
//						long subId = nodeMap.getKey(subNode);
//						
//						if (fromRootDstIdEdgeMap.keySet().contains(subId)){
//							edgeId = fromRootDstIdEdgeMap.get(subId).getId();
//						}else{
//							edgeId = actEdgeId++;
//						}
//						
//						addXmlEdge(getEdgeBit("decomposition"), rootId, subId, edgeId, edgeTypes, graph);
//					}
//				}
//			}else{
//				// do nothing
//			}		
//		}else{
//			long srcId = nodeMap.getKey(srcNode);
//			addXmlEdge(edgeBits, srcId, dstId, edgeId, edgeTypes, graph);
//		}
		
		
		String srcId = String.valueOf( srcNode.getId() );
		addXmlEdge(edgeBits, srcId, dstId, "-1", export.customEdgeTypes, export.graph);
		
	}

	protected void addXmlEdge(int edgeBits, String srcId, String dstId, String edgeId, 
			BidirectionalHashMap<Integer, String> edgeTypes, de.grogra.ext.exchangegraph.xmlbeans.Graph graph){
		if ((edgeBits & Graph.SUCCESSOR_EDGE) == Graph.SUCCESSOR_EDGE) {
			de.grogra.ext.exchangegraph.xmlbeans.Edge xmlEdge = graph
					.addNewEdge();
			xmlEdge.setId(edgeId);
			xmlEdge.setSrcId(srcId);
			xmlEdge.setDestId(dstId);
			xmlEdge.setType("successor");
		}
		if ((edgeBits & Graph.BRANCH_EDGE) == Graph.BRANCH_EDGE) {
			de.grogra.ext.exchangegraph.xmlbeans.Edge xmlEdge = graph
					.addNewEdge();
			xmlEdge.setId(edgeId);
			xmlEdge.setSrcId(srcId);
			xmlEdge.setDestId(dstId);
			xmlEdge.setType("branch");
		}
		
		if ((edgeBits & Graph.REFINEMENT_EDGE) == Graph.REFINEMENT_EDGE) {
			de.grogra.ext.exchangegraph.xmlbeans.Edge xmlEdge = graph
					.addNewEdge();
			xmlEdge.setId(edgeId);
			xmlEdge.setSrcId(srcId);
			xmlEdge.setDestId(dstId);
			xmlEdge.setType("decomposition");
		}
		
		for (int i = 0; i < 15; i++) {
			if (((edgeBits >> i) & Graph.MIN_UNUSED_EDGE) == Graph.MIN_UNUSED_EDGE) {
				de.grogra.ext.exchangegraph.xmlbeans.Edge xmlEdge = graph
						.addNewEdge();
				xmlEdge.setId(edgeId);
				xmlEdge.setSrcId(srcId);
				xmlEdge.setDestId(dstId);
				String edgeType = edgeTypes.get(i);
				if (edgeType == null)
					edgeType = "EDGE_" + String.valueOf(i);
				xmlEdge.setType(edgeType);
			}
		}
	}

}
