package de.grogra.ext.exchangegraph;

import java.io.File;
import java.io.IOException;
import java.io.Reader;
import java.util.HashMap;

import de.grogra.graph.impl.Node;
import de.grogra.pf.io.FileSource;
import de.grogra.pf.io.FilterBase;
import de.grogra.pf.io.FilterItem;
import de.grogra.pf.io.FilterSource;
import de.grogra.pf.io.IO;
import de.grogra.pf.io.IOFlavor;
import de.grogra.pf.io.ObjectSource;
import de.grogra.pf.io.ProjectLoader;
import de.grogra.pf.io.ReaderSource;
import de.grogra.pf.registry.Registry;
import de.grogra.pf.ui.Workbench;
import de.grogra.reflect.Type;
import de.grogra.vfs.LocalFileSystem;

public class XEGImportScene extends FilterBase implements ObjectSource, ProjectLoader, XEGImportContext{
	
	Workbench workbench;
	IOContext ctx;
	Node root;
	
	public XEGImportScene (FilterItem item, FilterSource source)
	{
		super (item, source);
		setFlavor (IOFlavor.PROJECT_LOADER);
	}
	
	@Override
	public void loadRegistry(Registry registry) throws IOException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void loadGraph(Registry registry) throws IOException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Object getObject() throws IOException {
		Object id = Workbench.get (source.getRegistry())
				.getApplication().create("newEmptyRGG", getSystemId());
		workbench = Workbench.get (source.getRegistry()).getApplication().getWorkbenchManager().getWorkbench(id);

		workbench.setFile(null, null);
		
		ctx = new IOContext();
		
		workbench.setProperty(IOContext.class.getName(), ctx);
		
		Reader reader = ((FileSource)source).getReader();

		root=getRegistry().getProjectGraph().getRoot();
		
		// load the modelTypes of the current project
		
		XEGImport importer = new XEGImport(reader, new HashMap<String, Type>(), this, true);
		importer.doImport();

		return this;
	}

	@Override
	public IOContext getIOContext() {
		return ctx;
	}

	@Override
	public Workbench getWorkbench() {
		return workbench;
	}

	@Override
	public Node getRoot() {
		return root;
	}
	
	@Override 
	public void setRoot(Node r) {
		root = r;
	}

}
