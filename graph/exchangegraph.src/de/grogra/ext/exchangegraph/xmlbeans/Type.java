/*
 * XML Type:  Type
 * Namespace: 
 * Java type: de.grogra.ext.exchangegraph.xmlbeans.Type
 *
 * Automatically generated - do not modify.
 */
package de.grogra.ext.exchangegraph.xmlbeans;

import org.apache.xmlbeans.impl.schema.ElementFactory;
import org.apache.xmlbeans.impl.schema.AbstractDocumentFactory;
import org.apache.xmlbeans.impl.schema.DocumentFactory;
import org.apache.xmlbeans.impl.schema.SimpleTypeFactory;


/**
 * An XML Type(@).
 *
 * This is a complex type.
 */
public interface Type extends org.apache.xmlbeans.XmlObject {
    DocumentFactory<de.grogra.ext.exchangegraph.xmlbeans.Type> Factory = new DocumentFactory<>(org.apache.xmlbeans.metadata.system.sE6B7510D6B33B7EC58169E8DC7B33B7F.TypeSystemHolder.typeSystem, "typeb143type");
    org.apache.xmlbeans.SchemaType type = Factory.getType();


    /**
     * Gets the "extends" element
     */
    de.grogra.ext.exchangegraph.xmlbeans.ExtendsType getExtends();

    /**
     * Sets the "extends" element
     */
    void setExtends(de.grogra.ext.exchangegraph.xmlbeans.ExtendsType xextends);

    /**
     * Appends and returns a new empty "extends" element
     */
    de.grogra.ext.exchangegraph.xmlbeans.ExtendsType addNewExtends();

    /**
     * Gets a List of "implements" elements
     */
    java.util.List<de.grogra.ext.exchangegraph.xmlbeans.ImplementsType> getImplementsList();

    /**
     * Gets array of all "implements" elements
     */
    de.grogra.ext.exchangegraph.xmlbeans.ImplementsType[] getImplementsArray();

    /**
     * Gets ith "implements" element
     */
    de.grogra.ext.exchangegraph.xmlbeans.ImplementsType getImplementsArray(int i);

    /**
     * Returns number of "implements" element
     */
    int sizeOfImplementsArray();

    /**
     * Sets array of all "implements" element
     */
    void setImplementsArray(de.grogra.ext.exchangegraph.xmlbeans.ImplementsType[] ximplementsArray);

    /**
     * Sets ith "implements" element
     */
    void setImplementsArray(int i, de.grogra.ext.exchangegraph.xmlbeans.ImplementsType ximplements);

    /**
     * Inserts and returns a new empty value (as xml) as the ith "implements" element
     */
    de.grogra.ext.exchangegraph.xmlbeans.ImplementsType insertNewImplements(int i);

    /**
     * Appends and returns a new empty value (as xml) as the last "implements" element
     */
    de.grogra.ext.exchangegraph.xmlbeans.ImplementsType addNewImplements();

    /**
     * Removes the ith "implements" element
     */
    void removeImplements(int i);

    /**
     * Gets a List of "property" elements
     */
    java.util.List<de.grogra.ext.exchangegraph.xmlbeans.Property> getPropertyList();

    /**
     * Gets array of all "property" elements
     */
    de.grogra.ext.exchangegraph.xmlbeans.Property[] getPropertyArray();

    /**
     * Gets ith "property" element
     */
    de.grogra.ext.exchangegraph.xmlbeans.Property getPropertyArray(int i);

    /**
     * Returns number of "property" element
     */
    int sizeOfPropertyArray();

    /**
     * Sets array of all "property" element
     */
    void setPropertyArray(de.grogra.ext.exchangegraph.xmlbeans.Property[] propertyArray);

    /**
     * Sets ith "property" element
     */
    void setPropertyArray(int i, de.grogra.ext.exchangegraph.xmlbeans.Property property);

    /**
     * Inserts and returns a new empty value (as xml) as the ith "property" element
     */
    de.grogra.ext.exchangegraph.xmlbeans.Property insertNewProperty(int i);

    /**
     * Appends and returns a new empty value (as xml) as the last "property" element
     */
    de.grogra.ext.exchangegraph.xmlbeans.Property addNewProperty();

    /**
     * Removes the ith "property" element
     */
    void removeProperty(int i);

    /**
     * Gets the "name" attribute
     */
    java.lang.String getName();

    /**
     * Gets (as xml) the "name" attribute
     */
    org.apache.xmlbeans.XmlString xgetName();

    /**
     * True if has "name" attribute
     */
    boolean isSetName();

    /**
     * Sets the "name" attribute
     */
    void setName(java.lang.String name);

    /**
     * Sets (as xml) the "name" attribute
     */
    void xsetName(org.apache.xmlbeans.XmlString name);

    /**
     * Unsets the "name" attribute
     */
    void unsetName();
}
