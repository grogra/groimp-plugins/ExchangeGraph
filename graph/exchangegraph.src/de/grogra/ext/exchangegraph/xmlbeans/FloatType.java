/*
 * XML Type:  float_type
 * Namespace: 
 * Java type: de.grogra.ext.exchangegraph.xmlbeans.FloatType
 *
 * Automatically generated - do not modify.
 */
package de.grogra.ext.exchangegraph.xmlbeans;

import org.apache.xmlbeans.impl.schema.ElementFactory;
import org.apache.xmlbeans.impl.schema.AbstractDocumentFactory;
import org.apache.xmlbeans.impl.schema.DocumentFactory;
import org.apache.xmlbeans.impl.schema.SimpleTypeFactory;


/**
 * An XML float_type(@).
 *
 * This is an atomic type that is a restriction of de.grogra.ext.exchangegraph.xmlbeans.FloatType.
 */
public interface FloatType extends org.apache.xmlbeans.XmlFloat {
    SimpleTypeFactory<de.grogra.ext.exchangegraph.xmlbeans.FloatType> Factory = new SimpleTypeFactory<>(org.apache.xmlbeans.metadata.system.sE6B7510D6B33B7EC58169E8DC7B33B7F.TypeSystemHolder.typeSystem, "floattype1a66type");
    org.apache.xmlbeans.SchemaType type = Factory.getType();

}
