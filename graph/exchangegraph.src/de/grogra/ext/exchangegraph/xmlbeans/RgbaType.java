/*
 * XML Type:  rgba_type
 * Namespace: 
 * Java type: de.grogra.ext.exchangegraph.xmlbeans.RgbaType
 *
 * Automatically generated - do not modify.
 */
package de.grogra.ext.exchangegraph.xmlbeans;

import org.apache.xmlbeans.impl.schema.ElementFactory;
import org.apache.xmlbeans.impl.schema.AbstractDocumentFactory;
import org.apache.xmlbeans.impl.schema.DocumentFactory;
import org.apache.xmlbeans.impl.schema.SimpleTypeFactory;


/**
 * An XML rgba_type(@).
 *
 * This is a list type whose items are de.grogra.ext.exchangegraph.xmlbeans.FloatType.
 */
public interface RgbaType extends de.grogra.ext.exchangegraph.xmlbeans.Float4X1Type {
    java.util.List getListValue();
    java.util.List xgetListValue();
    void setListValue(java.util.List<?> list);
    SimpleTypeFactory<de.grogra.ext.exchangegraph.xmlbeans.RgbaType> Factory = new SimpleTypeFactory<>(org.apache.xmlbeans.metadata.system.sE6B7510D6B33B7EC58169E8DC7B33B7F.TypeSystemHolder.typeSystem, "rgbatype231ctype");
    org.apache.xmlbeans.SchemaType type = Factory.getType();

}
