/*
 * XML Type:  Edge
 * Namespace: 
 * Java type: de.grogra.ext.exchangegraph.xmlbeans.Edge
 *
 * Automatically generated - do not modify.
 */
package de.grogra.ext.exchangegraph.xmlbeans;

import org.apache.xmlbeans.impl.schema.ElementFactory;
import org.apache.xmlbeans.impl.schema.AbstractDocumentFactory;
import org.apache.xmlbeans.impl.schema.DocumentFactory;
import org.apache.xmlbeans.impl.schema.SimpleTypeFactory;


/**
 * An XML Edge(@).
 *
 * This is a complex type.
 */
public interface Edge extends org.apache.xmlbeans.XmlObject {
    DocumentFactory<de.grogra.ext.exchangegraph.xmlbeans.Edge> Factory = new DocumentFactory<>(org.apache.xmlbeans.metadata.system.sE6B7510D6B33B7EC58169E8DC7B33B7F.TypeSystemHolder.typeSystem, "edge8fc6type");
    org.apache.xmlbeans.SchemaType type = Factory.getType();


    /**
     * Gets the "id" attribute
     */
    java.lang.String getId();

    /**
     * Gets (as xml) the "id" attribute
     */
    de.grogra.ext.exchangegraph.xmlbeans.IdType xgetId();

    /**
     * True if has "id" attribute
     */
    boolean isSetId();

    /**
     * Sets the "id" attribute
     */
    void setId(java.lang.String id);

    /**
     * Sets (as xml) the "id" attribute
     */
    void xsetId(de.grogra.ext.exchangegraph.xmlbeans.IdType id);

    /**
     * Unsets the "id" attribute
     */
    void unsetId();

    /**
     * Gets the "src_id" attribute
     */
    java.lang.String getSrcId();

    /**
     * Gets (as xml) the "src_id" attribute
     */
    de.grogra.ext.exchangegraph.xmlbeans.IdType xgetSrcId();

    /**
     * Sets the "src_id" attribute
     */
    void setSrcId(java.lang.String srcId);

    /**
     * Sets (as xml) the "src_id" attribute
     */
    void xsetSrcId(de.grogra.ext.exchangegraph.xmlbeans.IdType srcId);

    /**
     * Gets the "dest_id" attribute
     */
    java.lang.String getDestId();

    /**
     * Gets (as xml) the "dest_id" attribute
     */
    de.grogra.ext.exchangegraph.xmlbeans.IdType xgetDestId();

    /**
     * Sets the "dest_id" attribute
     */
    void setDestId(java.lang.String destId);

    /**
     * Sets (as xml) the "dest_id" attribute
     */
    void xsetDestId(de.grogra.ext.exchangegraph.xmlbeans.IdType destId);

    /**
     * Gets the "type" attribute
     */
    java.lang.String getType();

    /**
     * Gets (as xml) the "type" attribute
     */
    org.apache.xmlbeans.XmlString xgetType();

    /**
     * Sets the "type" attribute
     */
    void setType(java.lang.String type);

    /**
     * Sets (as xml) the "type" attribute
     */
    void xsetType(org.apache.xmlbeans.XmlString type);
}
