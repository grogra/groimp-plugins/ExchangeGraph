/*
 * XML Type:  rgb_type
 * Namespace: 
 * Java type: de.grogra.ext.exchangegraph.xmlbeans.RgbType
 *
 * Automatically generated - do not modify.
 */
package de.grogra.ext.exchangegraph.xmlbeans;

import org.apache.xmlbeans.impl.schema.ElementFactory;
import org.apache.xmlbeans.impl.schema.AbstractDocumentFactory;
import org.apache.xmlbeans.impl.schema.DocumentFactory;
import org.apache.xmlbeans.impl.schema.SimpleTypeFactory;


/**
 * An XML rgb_type(@).
 *
 * This is a list type whose items are de.grogra.ext.exchangegraph.xmlbeans.FloatType.
 */
public interface RgbType extends de.grogra.ext.exchangegraph.xmlbeans.Float3X1Type {
    java.util.List getListValue();
    java.util.List xgetListValue();
    void setListValue(java.util.List<?> list);
    SimpleTypeFactory<de.grogra.ext.exchangegraph.xmlbeans.RgbType> Factory = new SimpleTypeFactory<>(org.apache.xmlbeans.metadata.system.sE6B7510D6B33B7EC58169E8DC7B33B7F.TypeSystemHolder.typeSystem, "rgbtype98d5type");
    org.apache.xmlbeans.SchemaType type = Factory.getType();

}
