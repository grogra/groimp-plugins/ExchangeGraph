/*
 * An XML document type.
 * Localname: graph
 * Namespace: 
 * Java type: de.grogra.ext.exchangegraph.xmlbeans.GraphDocument
 *
 * Automatically generated - do not modify.
 */
package de.grogra.ext.exchangegraph.xmlbeans;

import org.apache.xmlbeans.impl.schema.ElementFactory;
import org.apache.xmlbeans.impl.schema.AbstractDocumentFactory;
import org.apache.xmlbeans.impl.schema.DocumentFactory;
import org.apache.xmlbeans.impl.schema.SimpleTypeFactory;


/**
 * A document containing one graph(@) element.
 *
 * This is a complex type.
 */
public interface GraphDocument extends org.apache.xmlbeans.XmlObject {
    DocumentFactory<de.grogra.ext.exchangegraph.xmlbeans.GraphDocument> Factory = new DocumentFactory<>(org.apache.xmlbeans.metadata.system.sE6B7510D6B33B7EC58169E8DC7B33B7F.TypeSystemHolder.typeSystem, "graph1c75doctype");
    org.apache.xmlbeans.SchemaType type = Factory.getType();


    /**
     * Gets the "graph" element
     */
    de.grogra.ext.exchangegraph.xmlbeans.Graph getGraph();

    /**
     * Sets the "graph" element
     */
    void setGraph(de.grogra.ext.exchangegraph.xmlbeans.Graph graph);

    /**
     * Appends and returns a new empty "graph" element
     */
    de.grogra.ext.exchangegraph.xmlbeans.Graph addNewGraph();
}
