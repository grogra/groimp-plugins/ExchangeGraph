/*
 * XML Type:  float_type
 * Namespace: 
 * Java type: de.grogra.ext.exchangegraph.xmlbeans.FloatType
 *
 * Automatically generated - do not modify.
 */
package de.grogra.ext.exchangegraph.xmlbeans.impl;

import javax.xml.namespace.QName;
import org.apache.xmlbeans.QNameSet;
import org.apache.xmlbeans.XmlObject;

/**
 * An XML float_type(@).
 *
 * This is an atomic type that is a restriction of de.grogra.ext.exchangegraph.xmlbeans.FloatType.
 */
public class FloatTypeImpl extends org.apache.xmlbeans.impl.values.JavaFloatHolderEx implements de.grogra.ext.exchangegraph.xmlbeans.FloatType {
    private static final long serialVersionUID = 1L;

    public FloatTypeImpl(org.apache.xmlbeans.SchemaType sType) {
        super(sType, false);
    }

    protected FloatTypeImpl(org.apache.xmlbeans.SchemaType sType, boolean b) {
        super(sType, b);
    }
}
