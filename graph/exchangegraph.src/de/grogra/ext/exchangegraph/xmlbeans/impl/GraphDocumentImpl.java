/*
 * An XML document type.
 * Localname: graph
 * Namespace: 
 * Java type: de.grogra.ext.exchangegraph.xmlbeans.GraphDocument
 *
 * Automatically generated - do not modify.
 */
package de.grogra.ext.exchangegraph.xmlbeans.impl;

import javax.xml.namespace.QName;
import org.apache.xmlbeans.QNameSet;
import org.apache.xmlbeans.XmlObject;

/**
 * A document containing one graph(@) element.
 *
 * This is a complex type.
 */
public class GraphDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements de.grogra.ext.exchangegraph.xmlbeans.GraphDocument {
    private static final long serialVersionUID = 1L;

    public GraphDocumentImpl(org.apache.xmlbeans.SchemaType sType) {
        super(sType);
    }

    private static final QName[] PROPERTY_QNAME = {
        new QName("", "graph"),
    };


    /**
     * Gets the "graph" element
     */
    @Override
    public de.grogra.ext.exchangegraph.xmlbeans.Graph getGraph() {
        synchronized (monitor()) {
            check_orphaned();
            de.grogra.ext.exchangegraph.xmlbeans.Graph target = null;
            target = (de.grogra.ext.exchangegraph.xmlbeans.Graph)get_store().find_element_user(PROPERTY_QNAME[0], 0);
            return (target == null) ? null : target;
        }
    }

    /**
     * Sets the "graph" element
     */
    @Override
    public void setGraph(de.grogra.ext.exchangegraph.xmlbeans.Graph graph) {
        generatedSetterHelperImpl(graph, PROPERTY_QNAME[0], 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
    }

    /**
     * Appends and returns a new empty "graph" element
     */
    @Override
    public de.grogra.ext.exchangegraph.xmlbeans.Graph addNewGraph() {
        synchronized (monitor()) {
            check_orphaned();
            de.grogra.ext.exchangegraph.xmlbeans.Graph target = null;
            target = (de.grogra.ext.exchangegraph.xmlbeans.Graph)get_store().add_element_user(PROPERTY_QNAME[0]);
            return target;
        }
    }
}
