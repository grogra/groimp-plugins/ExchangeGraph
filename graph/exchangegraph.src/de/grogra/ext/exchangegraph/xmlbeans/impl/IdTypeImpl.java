/*
 * XML Type:  id_type
 * Namespace: 
 * Java type: de.grogra.ext.exchangegraph.xmlbeans.IdType
 *
 * Automatically generated - do not modify.
 */
package de.grogra.ext.exchangegraph.xmlbeans.impl;

import javax.xml.namespace.QName;
import org.apache.xmlbeans.QNameSet;
import org.apache.xmlbeans.XmlObject;

/**
 * An XML id_type(@).
 *
 * This is an atomic type that is a restriction of de.grogra.ext.exchangegraph.xmlbeans.IdType.
 */
public class IdTypeImpl extends org.apache.xmlbeans.impl.values.JavaStringHolderEx implements de.grogra.ext.exchangegraph.xmlbeans.IdType {
    private static final long serialVersionUID = 1L;

    public IdTypeImpl(org.apache.xmlbeans.SchemaType sType) {
        super(sType, false);
    }

    protected IdTypeImpl(org.apache.xmlbeans.SchemaType sType, boolean b) {
        super(sType, b);
    }
}
