/*
 * XML Type:  list_of_floats_type
 * Namespace: 
 * Java type: de.grogra.ext.exchangegraph.xmlbeans.ListOfFloatsType
 *
 * Automatically generated - do not modify.
 */
package de.grogra.ext.exchangegraph.xmlbeans.impl;
/**
 * An XML list_of_floats_type(@).
 *
 * This is a list type whose items are de.grogra.ext.exchangegraph.xmlbeans.FloatType.
 */
public class ListOfFloatsTypeImpl extends org.apache.xmlbeans.impl.values.XmlListImpl implements de.grogra.ext.exchangegraph.xmlbeans.ListOfFloatsType
{
    
    public ListOfFloatsTypeImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType, false);
    }
    
    protected ListOfFloatsTypeImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
    {
        super(sType, b);
    }
}
