/*
 * XML Type:  Graph
 * Namespace: 
 * Java type: de.grogra.ext.exchangegraph.xmlbeans.Graph
 *
 * Automatically generated - do not modify.
 */
package de.grogra.ext.exchangegraph.xmlbeans.impl;

import javax.xml.namespace.QName;
import org.apache.xmlbeans.QNameSet;
import org.apache.xmlbeans.XmlObject;

/**
 * An XML Graph(@).
 *
 * This is a complex type.
 */
public class GraphImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements de.grogra.ext.exchangegraph.xmlbeans.Graph {
    private static final long serialVersionUID = 1L;

    public GraphImpl(org.apache.xmlbeans.SchemaType sType) {
        super(sType);
    }

    private static final QName[] PROPERTY_QNAME = {
        new QName("", "type"),
        new QName("", "root"),
        new QName("", "node"),
        new QName("", "edge"),
    };


    /**
     * Gets a List of "type" elements
     */
    @Override
    public java.util.List<de.grogra.ext.exchangegraph.xmlbeans.Type> getTypeList() {
        synchronized (monitor()) {
            check_orphaned();
            return new org.apache.xmlbeans.impl.values.JavaListXmlObject<>(
                this::getTypeArray,
                this::setTypeArray,
                this::insertNewType,
                this::removeType,
                this::sizeOfTypeArray
            );
        }
    }

    /**
     * Gets array of all "type" elements
     */
    @Override
    public de.grogra.ext.exchangegraph.xmlbeans.Type[] getTypeArray() {
        return getXmlObjectArray(PROPERTY_QNAME[0], new de.grogra.ext.exchangegraph.xmlbeans.Type[0]);
    }

    /**
     * Gets ith "type" element
     */
    @Override
    public de.grogra.ext.exchangegraph.xmlbeans.Type getTypeArray(int i) {
        synchronized (monitor()) {
            check_orphaned();
            de.grogra.ext.exchangegraph.xmlbeans.Type target = null;
            target = (de.grogra.ext.exchangegraph.xmlbeans.Type)get_store().find_element_user(PROPERTY_QNAME[0], i);
            if (target == null) {
                throw new IndexOutOfBoundsException();
            }
            return target;
        }
    }

    /**
     * Returns number of "type" element
     */
    @Override
    public int sizeOfTypeArray() {
        synchronized (monitor()) {
            check_orphaned();
            return get_store().count_elements(PROPERTY_QNAME[0]);
        }
    }

    /**
     * Sets array of all "type" element  WARNING: This method is not atomicaly synchronized.
     */
    @Override
    public void setTypeArray(de.grogra.ext.exchangegraph.xmlbeans.Type[] typeArray) {
        check_orphaned();
        arraySetterHelper(typeArray, PROPERTY_QNAME[0]);
    }

    /**
     * Sets ith "type" element
     */
    @Override
    public void setTypeArray(int i, de.grogra.ext.exchangegraph.xmlbeans.Type type) {
        generatedSetterHelperImpl(type, PROPERTY_QNAME[0], i, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_ARRAYITEM);
    }

    /**
     * Inserts and returns a new empty value (as xml) as the ith "type" element
     */
    @Override
    public de.grogra.ext.exchangegraph.xmlbeans.Type insertNewType(int i) {
        synchronized (monitor()) {
            check_orphaned();
            de.grogra.ext.exchangegraph.xmlbeans.Type target = null;
            target = (de.grogra.ext.exchangegraph.xmlbeans.Type)get_store().insert_element_user(PROPERTY_QNAME[0], i);
            return target;
        }
    }

    /**
     * Appends and returns a new empty value (as xml) as the last "type" element
     */
    @Override
    public de.grogra.ext.exchangegraph.xmlbeans.Type addNewType() {
        synchronized (monitor()) {
            check_orphaned();
            de.grogra.ext.exchangegraph.xmlbeans.Type target = null;
            target = (de.grogra.ext.exchangegraph.xmlbeans.Type)get_store().add_element_user(PROPERTY_QNAME[0]);
            return target;
        }
    }

    /**
     * Removes the ith "type" element
     */
    @Override
    public void removeType(int i) {
        synchronized (monitor()) {
            check_orphaned();
            get_store().remove_element(PROPERTY_QNAME[0], i);
        }
    }

    /**
     * Gets a List of "root" elements
     */
    @Override
    public java.util.List<de.grogra.ext.exchangegraph.xmlbeans.Root> getRootList() {
        synchronized (monitor()) {
            check_orphaned();
            return new org.apache.xmlbeans.impl.values.JavaListXmlObject<>(
                this::getRootArray,
                this::setRootArray,
                this::insertNewRoot,
                this::removeRoot,
                this::sizeOfRootArray
            );
        }
    }

    /**
     * Gets array of all "root" elements
     */
    @Override
    public de.grogra.ext.exchangegraph.xmlbeans.Root[] getRootArray() {
        return getXmlObjectArray(PROPERTY_QNAME[1], new de.grogra.ext.exchangegraph.xmlbeans.Root[0]);
    }

    /**
     * Gets ith "root" element
     */
    @Override
    public de.grogra.ext.exchangegraph.xmlbeans.Root getRootArray(int i) {
        synchronized (monitor()) {
            check_orphaned();
            de.grogra.ext.exchangegraph.xmlbeans.Root target = null;
            target = (de.grogra.ext.exchangegraph.xmlbeans.Root)get_store().find_element_user(PROPERTY_QNAME[1], i);
            if (target == null) {
                throw new IndexOutOfBoundsException();
            }
            return target;
        }
    }

    /**
     * Returns number of "root" element
     */
    @Override
    public int sizeOfRootArray() {
        synchronized (monitor()) {
            check_orphaned();
            return get_store().count_elements(PROPERTY_QNAME[1]);
        }
    }

    /**
     * Sets array of all "root" element  WARNING: This method is not atomicaly synchronized.
     */
    @Override
    public void setRootArray(de.grogra.ext.exchangegraph.xmlbeans.Root[] rootArray) {
        check_orphaned();
        arraySetterHelper(rootArray, PROPERTY_QNAME[1]);
    }

    /**
     * Sets ith "root" element
     */
    @Override
    public void setRootArray(int i, de.grogra.ext.exchangegraph.xmlbeans.Root root) {
        generatedSetterHelperImpl(root, PROPERTY_QNAME[1], i, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_ARRAYITEM);
    }

    /**
     * Inserts and returns a new empty value (as xml) as the ith "root" element
     */
    @Override
    public de.grogra.ext.exchangegraph.xmlbeans.Root insertNewRoot(int i) {
        synchronized (monitor()) {
            check_orphaned();
            de.grogra.ext.exchangegraph.xmlbeans.Root target = null;
            target = (de.grogra.ext.exchangegraph.xmlbeans.Root)get_store().insert_element_user(PROPERTY_QNAME[1], i);
            return target;
        }
    }

    /**
     * Appends and returns a new empty value (as xml) as the last "root" element
     */
    @Override
    public de.grogra.ext.exchangegraph.xmlbeans.Root addNewRoot() {
        synchronized (monitor()) {
            check_orphaned();
            de.grogra.ext.exchangegraph.xmlbeans.Root target = null;
            target = (de.grogra.ext.exchangegraph.xmlbeans.Root)get_store().add_element_user(PROPERTY_QNAME[1]);
            return target;
        }
    }

    /**
     * Removes the ith "root" element
     */
    @Override
    public void removeRoot(int i) {
        synchronized (monitor()) {
            check_orphaned();
            get_store().remove_element(PROPERTY_QNAME[1], i);
        }
    }

    /**
     * Gets a List of "node" elements
     */
    @Override
    public java.util.List<de.grogra.ext.exchangegraph.xmlbeans.Node> getNodeList() {
        synchronized (monitor()) {
            check_orphaned();
            return new org.apache.xmlbeans.impl.values.JavaListXmlObject<>(
                this::getNodeArray,
                this::setNodeArray,
                this::insertNewNode,
                this::removeNode,
                this::sizeOfNodeArray
            );
        }
    }

    /**
     * Gets array of all "node" elements
     */
    @Override
    public de.grogra.ext.exchangegraph.xmlbeans.Node[] getNodeArray() {
        return getXmlObjectArray(PROPERTY_QNAME[2], new de.grogra.ext.exchangegraph.xmlbeans.Node[0]);
    }

    /**
     * Gets ith "node" element
     */
    @Override
    public de.grogra.ext.exchangegraph.xmlbeans.Node getNodeArray(int i) {
        synchronized (monitor()) {
            check_orphaned();
            de.grogra.ext.exchangegraph.xmlbeans.Node target = null;
            target = (de.grogra.ext.exchangegraph.xmlbeans.Node)get_store().find_element_user(PROPERTY_QNAME[2], i);
            if (target == null) {
                throw new IndexOutOfBoundsException();
            }
            return target;
        }
    }

    /**
     * Returns number of "node" element
     */
    @Override
    public int sizeOfNodeArray() {
        synchronized (monitor()) {
            check_orphaned();
            return get_store().count_elements(PROPERTY_QNAME[2]);
        }
    }

    /**
     * Sets array of all "node" element  WARNING: This method is not atomicaly synchronized.
     */
    @Override
    public void setNodeArray(de.grogra.ext.exchangegraph.xmlbeans.Node[] nodeArray) {
        check_orphaned();
        arraySetterHelper(nodeArray, PROPERTY_QNAME[2]);
    }

    /**
     * Sets ith "node" element
     */
    @Override
    public void setNodeArray(int i, de.grogra.ext.exchangegraph.xmlbeans.Node node) {
        generatedSetterHelperImpl(node, PROPERTY_QNAME[2], i, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_ARRAYITEM);
    }

    /**
     * Inserts and returns a new empty value (as xml) as the ith "node" element
     */
    @Override
    public de.grogra.ext.exchangegraph.xmlbeans.Node insertNewNode(int i) {
        synchronized (monitor()) {
            check_orphaned();
            de.grogra.ext.exchangegraph.xmlbeans.Node target = null;
            target = (de.grogra.ext.exchangegraph.xmlbeans.Node)get_store().insert_element_user(PROPERTY_QNAME[2], i);
            return target;
        }
    }

    /**
     * Appends and returns a new empty value (as xml) as the last "node" element
     */
    @Override
    public de.grogra.ext.exchangegraph.xmlbeans.Node addNewNode() {
        synchronized (monitor()) {
            check_orphaned();
            de.grogra.ext.exchangegraph.xmlbeans.Node target = null;
            target = (de.grogra.ext.exchangegraph.xmlbeans.Node)get_store().add_element_user(PROPERTY_QNAME[2]);
            return target;
        }
    }

    /**
     * Removes the ith "node" element
     */
    @Override
    public void removeNode(int i) {
        synchronized (monitor()) {
            check_orphaned();
            get_store().remove_element(PROPERTY_QNAME[2], i);
        }
    }

    /**
     * Gets a List of "edge" elements
     */
    @Override
    public java.util.List<de.grogra.ext.exchangegraph.xmlbeans.Edge> getEdgeList() {
        synchronized (monitor()) {
            check_orphaned();
            return new org.apache.xmlbeans.impl.values.JavaListXmlObject<>(
                this::getEdgeArray,
                this::setEdgeArray,
                this::insertNewEdge,
                this::removeEdge,
                this::sizeOfEdgeArray
            );
        }
    }

    /**
     * Gets array of all "edge" elements
     */
    @Override
    public de.grogra.ext.exchangegraph.xmlbeans.Edge[] getEdgeArray() {
        return getXmlObjectArray(PROPERTY_QNAME[3], new de.grogra.ext.exchangegraph.xmlbeans.Edge[0]);
    }

    /**
     * Gets ith "edge" element
     */
    @Override
    public de.grogra.ext.exchangegraph.xmlbeans.Edge getEdgeArray(int i) {
        synchronized (monitor()) {
            check_orphaned();
            de.grogra.ext.exchangegraph.xmlbeans.Edge target = null;
            target = (de.grogra.ext.exchangegraph.xmlbeans.Edge)get_store().find_element_user(PROPERTY_QNAME[3], i);
            if (target == null) {
                throw new IndexOutOfBoundsException();
            }
            return target;
        }
    }

    /**
     * Returns number of "edge" element
     */
    @Override
    public int sizeOfEdgeArray() {
        synchronized (monitor()) {
            check_orphaned();
            return get_store().count_elements(PROPERTY_QNAME[3]);
        }
    }

    /**
     * Sets array of all "edge" element  WARNING: This method is not atomicaly synchronized.
     */
    @Override
    public void setEdgeArray(de.grogra.ext.exchangegraph.xmlbeans.Edge[] edgeArray) {
        check_orphaned();
        arraySetterHelper(edgeArray, PROPERTY_QNAME[3]);
    }

    /**
     * Sets ith "edge" element
     */
    @Override
    public void setEdgeArray(int i, de.grogra.ext.exchangegraph.xmlbeans.Edge edge) {
        generatedSetterHelperImpl(edge, PROPERTY_QNAME[3], i, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_ARRAYITEM);
    }

    /**
     * Inserts and returns a new empty value (as xml) as the ith "edge" element
     */
    @Override
    public de.grogra.ext.exchangegraph.xmlbeans.Edge insertNewEdge(int i) {
        synchronized (monitor()) {
            check_orphaned();
            de.grogra.ext.exchangegraph.xmlbeans.Edge target = null;
            target = (de.grogra.ext.exchangegraph.xmlbeans.Edge)get_store().insert_element_user(PROPERTY_QNAME[3], i);
            return target;
        }
    }

    /**
     * Appends and returns a new empty value (as xml) as the last "edge" element
     */
    @Override
    public de.grogra.ext.exchangegraph.xmlbeans.Edge addNewEdge() {
        synchronized (monitor()) {
            check_orphaned();
            de.grogra.ext.exchangegraph.xmlbeans.Edge target = null;
            target = (de.grogra.ext.exchangegraph.xmlbeans.Edge)get_store().add_element_user(PROPERTY_QNAME[3]);
            return target;
        }
    }

    /**
     * Removes the ith "edge" element
     */
    @Override
    public void removeEdge(int i) {
        synchronized (monitor()) {
            check_orphaned();
            get_store().remove_element(PROPERTY_QNAME[3], i);
        }
    }
}
