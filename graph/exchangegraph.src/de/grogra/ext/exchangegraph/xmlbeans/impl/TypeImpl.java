/*
 * XML Type:  Type
 * Namespace: 
 * Java type: de.grogra.ext.exchangegraph.xmlbeans.Type
 *
 * Automatically generated - do not modify.
 */
package de.grogra.ext.exchangegraph.xmlbeans.impl;

import javax.xml.namespace.QName;
import org.apache.xmlbeans.QNameSet;
import org.apache.xmlbeans.XmlObject;

/**
 * An XML Type(@).
 *
 * This is a complex type.
 */
public class TypeImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements de.grogra.ext.exchangegraph.xmlbeans.Type {
    private static final long serialVersionUID = 1L;

    public TypeImpl(org.apache.xmlbeans.SchemaType sType) {
        super(sType);
    }

    private static final QName[] PROPERTY_QNAME = {
        new QName("", "extends"),
        new QName("", "implements"),
        new QName("", "property"),
        new QName("", "name"),
    };


    /**
     * Gets the "extends" element
     */
    @Override
    public de.grogra.ext.exchangegraph.xmlbeans.ExtendsType getExtends() {
        synchronized (monitor()) {
            check_orphaned();
            de.grogra.ext.exchangegraph.xmlbeans.ExtendsType target = null;
            target = (de.grogra.ext.exchangegraph.xmlbeans.ExtendsType)get_store().find_element_user(PROPERTY_QNAME[0], 0);
            return (target == null) ? null : target;
        }
    }

    /**
     * Sets the "extends" element
     */
    @Override
    public void setExtends(de.grogra.ext.exchangegraph.xmlbeans.ExtendsType xextends) {
        generatedSetterHelperImpl(xextends, PROPERTY_QNAME[0], 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
    }

    /**
     * Appends and returns a new empty "extends" element
     */
    @Override
    public de.grogra.ext.exchangegraph.xmlbeans.ExtendsType addNewExtends() {
        synchronized (monitor()) {
            check_orphaned();
            de.grogra.ext.exchangegraph.xmlbeans.ExtendsType target = null;
            target = (de.grogra.ext.exchangegraph.xmlbeans.ExtendsType)get_store().add_element_user(PROPERTY_QNAME[0]);
            return target;
        }
    }

    /**
     * Gets a List of "implements" elements
     */
    @Override
    public java.util.List<de.grogra.ext.exchangegraph.xmlbeans.ImplementsType> getImplementsList() {
        synchronized (monitor()) {
            check_orphaned();
            return new org.apache.xmlbeans.impl.values.JavaListXmlObject<>(
                this::getImplementsArray,
                this::setImplementsArray,
                this::insertNewImplements,
                this::removeImplements,
                this::sizeOfImplementsArray
            );
        }
    }

    /**
     * Gets array of all "implements" elements
     */
    @Override
    public de.grogra.ext.exchangegraph.xmlbeans.ImplementsType[] getImplementsArray() {
        return getXmlObjectArray(PROPERTY_QNAME[1], new de.grogra.ext.exchangegraph.xmlbeans.ImplementsType[0]);
    }

    /**
     * Gets ith "implements" element
     */
    @Override
    public de.grogra.ext.exchangegraph.xmlbeans.ImplementsType getImplementsArray(int i) {
        synchronized (monitor()) {
            check_orphaned();
            de.grogra.ext.exchangegraph.xmlbeans.ImplementsType target = null;
            target = (de.grogra.ext.exchangegraph.xmlbeans.ImplementsType)get_store().find_element_user(PROPERTY_QNAME[1], i);
            if (target == null) {
                throw new IndexOutOfBoundsException();
            }
            return target;
        }
    }

    /**
     * Returns number of "implements" element
     */
    @Override
    public int sizeOfImplementsArray() {
        synchronized (monitor()) {
            check_orphaned();
            return get_store().count_elements(PROPERTY_QNAME[1]);
        }
    }

    /**
     * Sets array of all "implements" element  WARNING: This method is not atomicaly synchronized.
     */
    @Override
    public void setImplementsArray(de.grogra.ext.exchangegraph.xmlbeans.ImplementsType[] ximplementsArray) {
        check_orphaned();
        arraySetterHelper(ximplementsArray, PROPERTY_QNAME[1]);
    }

    /**
     * Sets ith "implements" element
     */
    @Override
    public void setImplementsArray(int i, de.grogra.ext.exchangegraph.xmlbeans.ImplementsType ximplements) {
        generatedSetterHelperImpl(ximplements, PROPERTY_QNAME[1], i, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_ARRAYITEM);
    }

    /**
     * Inserts and returns a new empty value (as xml) as the ith "implements" element
     */
    @Override
    public de.grogra.ext.exchangegraph.xmlbeans.ImplementsType insertNewImplements(int i) {
        synchronized (monitor()) {
            check_orphaned();
            de.grogra.ext.exchangegraph.xmlbeans.ImplementsType target = null;
            target = (de.grogra.ext.exchangegraph.xmlbeans.ImplementsType)get_store().insert_element_user(PROPERTY_QNAME[1], i);
            return target;
        }
    }

    /**
     * Appends and returns a new empty value (as xml) as the last "implements" element
     */
    @Override
    public de.grogra.ext.exchangegraph.xmlbeans.ImplementsType addNewImplements() {
        synchronized (monitor()) {
            check_orphaned();
            de.grogra.ext.exchangegraph.xmlbeans.ImplementsType target = null;
            target = (de.grogra.ext.exchangegraph.xmlbeans.ImplementsType)get_store().add_element_user(PROPERTY_QNAME[1]);
            return target;
        }
    }

    /**
     * Removes the ith "implements" element
     */
    @Override
    public void removeImplements(int i) {
        synchronized (monitor()) {
            check_orphaned();
            get_store().remove_element(PROPERTY_QNAME[1], i);
        }
    }

    /**
     * Gets a List of "property" elements
     */
    @Override
    public java.util.List<de.grogra.ext.exchangegraph.xmlbeans.Property> getPropertyList() {
        synchronized (monitor()) {
            check_orphaned();
            return new org.apache.xmlbeans.impl.values.JavaListXmlObject<>(
                this::getPropertyArray,
                this::setPropertyArray,
                this::insertNewProperty,
                this::removeProperty,
                this::sizeOfPropertyArray
            );
        }
    }

    /**
     * Gets array of all "property" elements
     */
    @Override
    public de.grogra.ext.exchangegraph.xmlbeans.Property[] getPropertyArray() {
        return getXmlObjectArray(PROPERTY_QNAME[2], new de.grogra.ext.exchangegraph.xmlbeans.Property[0]);
    }

    /**
     * Gets ith "property" element
     */
    @Override
    public de.grogra.ext.exchangegraph.xmlbeans.Property getPropertyArray(int i) {
        synchronized (monitor()) {
            check_orphaned();
            de.grogra.ext.exchangegraph.xmlbeans.Property target = null;
            target = (de.grogra.ext.exchangegraph.xmlbeans.Property)get_store().find_element_user(PROPERTY_QNAME[2], i);
            if (target == null) {
                throw new IndexOutOfBoundsException();
            }
            return target;
        }
    }

    /**
     * Returns number of "property" element
     */
    @Override
    public int sizeOfPropertyArray() {
        synchronized (monitor()) {
            check_orphaned();
            return get_store().count_elements(PROPERTY_QNAME[2]);
        }
    }

    /**
     * Sets array of all "property" element  WARNING: This method is not atomicaly synchronized.
     */
    @Override
    public void setPropertyArray(de.grogra.ext.exchangegraph.xmlbeans.Property[] propertyArray) {
        check_orphaned();
        arraySetterHelper(propertyArray, PROPERTY_QNAME[2]);
    }

    /**
     * Sets ith "property" element
     */
    @Override
    public void setPropertyArray(int i, de.grogra.ext.exchangegraph.xmlbeans.Property property) {
        generatedSetterHelperImpl(property, PROPERTY_QNAME[2], i, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_ARRAYITEM);
    }

    /**
     * Inserts and returns a new empty value (as xml) as the ith "property" element
     */
    @Override
    public de.grogra.ext.exchangegraph.xmlbeans.Property insertNewProperty(int i) {
        synchronized (monitor()) {
            check_orphaned();
            de.grogra.ext.exchangegraph.xmlbeans.Property target = null;
            target = (de.grogra.ext.exchangegraph.xmlbeans.Property)get_store().insert_element_user(PROPERTY_QNAME[2], i);
            return target;
        }
    }

    /**
     * Appends and returns a new empty value (as xml) as the last "property" element
     */
    @Override
    public de.grogra.ext.exchangegraph.xmlbeans.Property addNewProperty() {
        synchronized (monitor()) {
            check_orphaned();
            de.grogra.ext.exchangegraph.xmlbeans.Property target = null;
            target = (de.grogra.ext.exchangegraph.xmlbeans.Property)get_store().add_element_user(PROPERTY_QNAME[2]);
            return target;
        }
    }

    /**
     * Removes the ith "property" element
     */
    @Override
    public void removeProperty(int i) {
        synchronized (monitor()) {
            check_orphaned();
            get_store().remove_element(PROPERTY_QNAME[2], i);
        }
    }

    /**
     * Gets the "name" attribute
     */
    @Override
    public java.lang.String getName() {
        synchronized (monitor()) {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_attribute_user(PROPERTY_QNAME[3]);
            return (target == null) ? null : target.getStringValue();
        }
    }

    /**
     * Gets (as xml) the "name" attribute
     */
    @Override
    public org.apache.xmlbeans.XmlString xgetName() {
        synchronized (monitor()) {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_attribute_user(PROPERTY_QNAME[3]);
            return target;
        }
    }

    /**
     * True if has "name" attribute
     */
    @Override
    public boolean isSetName() {
        synchronized (monitor()) {
            check_orphaned();
            return get_store().find_attribute_user(PROPERTY_QNAME[3]) != null;
        }
    }

    /**
     * Sets the "name" attribute
     */
    @Override
    public void setName(java.lang.String name) {
        synchronized (monitor()) {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_attribute_user(PROPERTY_QNAME[3]);
            if (target == null) {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_attribute_user(PROPERTY_QNAME[3]);
            }
            target.setStringValue(name);
        }
    }

    /**
     * Sets (as xml) the "name" attribute
     */
    @Override
    public void xsetName(org.apache.xmlbeans.XmlString name) {
        synchronized (monitor()) {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_attribute_user(PROPERTY_QNAME[3]);
            if (target == null) {
                target = (org.apache.xmlbeans.XmlString)get_store().add_attribute_user(PROPERTY_QNAME[3]);
            }
            target.set(name);
        }
    }

    /**
     * Unsets the "name" attribute
     */
    @Override
    public void unsetName() {
        synchronized (monitor()) {
            check_orphaned();
            get_store().remove_attribute(PROPERTY_QNAME[3]);
        }
    }
}
