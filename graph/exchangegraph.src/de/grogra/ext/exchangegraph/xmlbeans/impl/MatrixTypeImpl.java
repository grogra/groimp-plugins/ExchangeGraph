/*
 * XML Type:  matrix_type
 * Namespace: 
 * Java type: de.grogra.ext.exchangegraph.xmlbeans.MatrixType
 *
 * Automatically generated - do not modify.
 */
package de.grogra.ext.exchangegraph.xmlbeans.impl;

import javax.xml.namespace.QName;
import org.apache.xmlbeans.QNameSet;
import org.apache.xmlbeans.XmlObject;

/**
 * An XML matrix_type(@).
 *
 * This is a list type whose items are de.grogra.ext.exchangegraph.xmlbeans.FloatType.
 */
public class MatrixTypeImpl extends org.apache.xmlbeans.impl.values.XmlListImpl implements de.grogra.ext.exchangegraph.xmlbeans.MatrixType {
    private static final long serialVersionUID = 1L;

    public MatrixTypeImpl(org.apache.xmlbeans.SchemaType sType) {
        super(sType, false);
    }

    protected MatrixTypeImpl(org.apache.xmlbeans.SchemaType sType, boolean b) {
        super(sType, b);
    }
}
