/*
 * XML Type:  Edge
 * Namespace: 
 * Java type: de.grogra.ext.exchangegraph.xmlbeans.Edge
 *
 * Automatically generated - do not modify.
 */
package de.grogra.ext.exchangegraph.xmlbeans.impl;

import javax.xml.namespace.QName;
import org.apache.xmlbeans.QNameSet;
import org.apache.xmlbeans.XmlObject;

/**
 * An XML Edge(@).
 *
 * This is a complex type.
 */
public class EdgeImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements de.grogra.ext.exchangegraph.xmlbeans.Edge {
    private static final long serialVersionUID = 1L;

    public EdgeImpl(org.apache.xmlbeans.SchemaType sType) {
        super(sType);
    }

    private static final QName[] PROPERTY_QNAME = {
        new QName("", "id"),
        new QName("", "src_id"),
        new QName("", "dest_id"),
        new QName("", "type"),
    };


    /**
     * Gets the "id" attribute
     */
    @Override
    public java.lang.String getId() {
        synchronized (monitor()) {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_attribute_user(PROPERTY_QNAME[0]);
            return (target == null) ? null : target.getStringValue();
        }
    }

    /**
     * Gets (as xml) the "id" attribute
     */
    @Override
    public de.grogra.ext.exchangegraph.xmlbeans.IdType xgetId() {
        synchronized (monitor()) {
            check_orphaned();
            de.grogra.ext.exchangegraph.xmlbeans.IdType target = null;
            target = (de.grogra.ext.exchangegraph.xmlbeans.IdType)get_store().find_attribute_user(PROPERTY_QNAME[0]);
            return target;
        }
    }

    /**
     * True if has "id" attribute
     */
    @Override
    public boolean isSetId() {
        synchronized (monitor()) {
            check_orphaned();
            return get_store().find_attribute_user(PROPERTY_QNAME[0]) != null;
        }
    }

    /**
     * Sets the "id" attribute
     */
    @Override
    public void setId(java.lang.String id) {
        synchronized (monitor()) {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_attribute_user(PROPERTY_QNAME[0]);
            if (target == null) {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_attribute_user(PROPERTY_QNAME[0]);
            }
            target.setStringValue(id);
        }
    }

    /**
     * Sets (as xml) the "id" attribute
     */
    @Override
    public void xsetId(de.grogra.ext.exchangegraph.xmlbeans.IdType id) {
        synchronized (monitor()) {
            check_orphaned();
            de.grogra.ext.exchangegraph.xmlbeans.IdType target = null;
            target = (de.grogra.ext.exchangegraph.xmlbeans.IdType)get_store().find_attribute_user(PROPERTY_QNAME[0]);
            if (target == null) {
                target = (de.grogra.ext.exchangegraph.xmlbeans.IdType)get_store().add_attribute_user(PROPERTY_QNAME[0]);
            }
            target.set(id);
        }
    }

    /**
     * Unsets the "id" attribute
     */
    @Override
    public void unsetId() {
        synchronized (monitor()) {
            check_orphaned();
            get_store().remove_attribute(PROPERTY_QNAME[0]);
        }
    }

    /**
     * Gets the "src_id" attribute
     */
    @Override
    public java.lang.String getSrcId() {
        synchronized (monitor()) {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_attribute_user(PROPERTY_QNAME[1]);
            return (target == null) ? null : target.getStringValue();
        }
    }

    /**
     * Gets (as xml) the "src_id" attribute
     */
    @Override
    public de.grogra.ext.exchangegraph.xmlbeans.IdType xgetSrcId() {
        synchronized (monitor()) {
            check_orphaned();
            de.grogra.ext.exchangegraph.xmlbeans.IdType target = null;
            target = (de.grogra.ext.exchangegraph.xmlbeans.IdType)get_store().find_attribute_user(PROPERTY_QNAME[1]);
            return target;
        }
    }

    /**
     * Sets the "src_id" attribute
     */
    @Override
    public void setSrcId(java.lang.String srcId) {
        synchronized (monitor()) {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_attribute_user(PROPERTY_QNAME[1]);
            if (target == null) {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_attribute_user(PROPERTY_QNAME[1]);
            }
            target.setStringValue(srcId);
        }
    }

    /**
     * Sets (as xml) the "src_id" attribute
     */
    @Override
    public void xsetSrcId(de.grogra.ext.exchangegraph.xmlbeans.IdType srcId) {
        synchronized (monitor()) {
            check_orphaned();
            de.grogra.ext.exchangegraph.xmlbeans.IdType target = null;
            target = (de.grogra.ext.exchangegraph.xmlbeans.IdType)get_store().find_attribute_user(PROPERTY_QNAME[1]);
            if (target == null) {
                target = (de.grogra.ext.exchangegraph.xmlbeans.IdType)get_store().add_attribute_user(PROPERTY_QNAME[1]);
            }
            target.set(srcId);
        }
    }

    /**
     * Gets the "dest_id" attribute
     */
    @Override
    public java.lang.String getDestId() {
        synchronized (monitor()) {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_attribute_user(PROPERTY_QNAME[2]);
            return (target == null) ? null : target.getStringValue();
        }
    }

    /**
     * Gets (as xml) the "dest_id" attribute
     */
    @Override
    public de.grogra.ext.exchangegraph.xmlbeans.IdType xgetDestId() {
        synchronized (monitor()) {
            check_orphaned();
            de.grogra.ext.exchangegraph.xmlbeans.IdType target = null;
            target = (de.grogra.ext.exchangegraph.xmlbeans.IdType)get_store().find_attribute_user(PROPERTY_QNAME[2]);
            return target;
        }
    }

    /**
     * Sets the "dest_id" attribute
     */
    @Override
    public void setDestId(java.lang.String destId) {
        synchronized (monitor()) {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_attribute_user(PROPERTY_QNAME[2]);
            if (target == null) {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_attribute_user(PROPERTY_QNAME[2]);
            }
            target.setStringValue(destId);
        }
    }

    /**
     * Sets (as xml) the "dest_id" attribute
     */
    @Override
    public void xsetDestId(de.grogra.ext.exchangegraph.xmlbeans.IdType destId) {
        synchronized (monitor()) {
            check_orphaned();
            de.grogra.ext.exchangegraph.xmlbeans.IdType target = null;
            target = (de.grogra.ext.exchangegraph.xmlbeans.IdType)get_store().find_attribute_user(PROPERTY_QNAME[2]);
            if (target == null) {
                target = (de.grogra.ext.exchangegraph.xmlbeans.IdType)get_store().add_attribute_user(PROPERTY_QNAME[2]);
            }
            target.set(destId);
        }
    }

    /**
     * Gets the "type" attribute
     */
    @Override
    public java.lang.String getType() {
        synchronized (monitor()) {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_attribute_user(PROPERTY_QNAME[3]);
            return (target == null) ? null : target.getStringValue();
        }
    }

    /**
     * Gets (as xml) the "type" attribute
     */
    @Override
    public org.apache.xmlbeans.XmlString xgetType() {
        synchronized (monitor()) {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_attribute_user(PROPERTY_QNAME[3]);
            return target;
        }
    }

    /**
     * Sets the "type" attribute
     */
    @Override
    public void setType(java.lang.String type) {
        synchronized (monitor()) {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_attribute_user(PROPERTY_QNAME[3]);
            if (target == null) {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_attribute_user(PROPERTY_QNAME[3]);
            }
            target.setStringValue(type);
        }
    }

    /**
     * Sets (as xml) the "type" attribute
     */
    @Override
    public void xsetType(org.apache.xmlbeans.XmlString type) {
        synchronized (monitor()) {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_attribute_user(PROPERTY_QNAME[3]);
            if (target == null) {
                target = (org.apache.xmlbeans.XmlString)get_store().add_attribute_user(PROPERTY_QNAME[3]);
            }
            target.set(type);
        }
    }
}
