/*
 * XML Type:  Root
 * Namespace: 
 * Java type: de.grogra.ext.exchangegraph.xmlbeans.Root
 *
 * Automatically generated - do not modify.
 */
package de.grogra.ext.exchangegraph.xmlbeans.impl;

import javax.xml.namespace.QName;
import org.apache.xmlbeans.QNameSet;
import org.apache.xmlbeans.XmlObject;

/**
 * An XML Root(@).
 *
 * This is a complex type.
 */
public class RootImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements de.grogra.ext.exchangegraph.xmlbeans.Root {
    private static final long serialVersionUID = 1L;

    public RootImpl(org.apache.xmlbeans.SchemaType sType) {
        super(sType);
    }

    private static final QName[] PROPERTY_QNAME = {
        new QName("", "root_id"),
    };


    /**
     * Gets the "root_id" attribute
     */
    @Override
    public java.lang.String getRootId() {
        synchronized (monitor()) {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_attribute_user(PROPERTY_QNAME[0]);
            return (target == null) ? null : target.getStringValue();
        }
    }

    /**
     * Gets (as xml) the "root_id" attribute
     */
    @Override
    public de.grogra.ext.exchangegraph.xmlbeans.IdType xgetRootId() {
        synchronized (monitor()) {
            check_orphaned();
            de.grogra.ext.exchangegraph.xmlbeans.IdType target = null;
            target = (de.grogra.ext.exchangegraph.xmlbeans.IdType)get_store().find_attribute_user(PROPERTY_QNAME[0]);
            return target;
        }
    }

    /**
     * Sets the "root_id" attribute
     */
    @Override
    public void setRootId(java.lang.String rootId) {
        synchronized (monitor()) {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_attribute_user(PROPERTY_QNAME[0]);
            if (target == null) {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_attribute_user(PROPERTY_QNAME[0]);
            }
            target.setStringValue(rootId);
        }
    }

    /**
     * Sets (as xml) the "root_id" attribute
     */
    @Override
    public void xsetRootId(de.grogra.ext.exchangegraph.xmlbeans.IdType rootId) {
        synchronized (monitor()) {
            check_orphaned();
            de.grogra.ext.exchangegraph.xmlbeans.IdType target = null;
            target = (de.grogra.ext.exchangegraph.xmlbeans.IdType)get_store().find_attribute_user(PROPERTY_QNAME[0]);
            if (target == null) {
                target = (de.grogra.ext.exchangegraph.xmlbeans.IdType)get_store().add_attribute_user(PROPERTY_QNAME[0]);
            }
            target.set(rootId);
        }
    }
}
