/*
 * XML Type:  double_type
 * Namespace: 
 * Java type: de.grogra.ext.exchangegraph.xmlbeans.DoubleType
 *
 * Automatically generated - do not modify.
 */
package de.grogra.ext.exchangegraph.xmlbeans.impl;
/**
 * An XML double_type(@).
 *
 * This is an atomic type that is a restriction of de.grogra.ext.exchangegraph.xmlbeans.DoubleType.
 */
public class DoubleTypeImpl extends org.apache.xmlbeans.impl.values.JavaDoubleHolderEx implements de.grogra.ext.exchangegraph.xmlbeans.DoubleType
{
    
    public DoubleTypeImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType, false);
    }
    
    protected DoubleTypeImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
    {
        super(sType, b);
    }
}
