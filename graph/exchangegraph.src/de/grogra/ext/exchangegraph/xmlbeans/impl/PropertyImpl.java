/*
 * XML Type:  Property
 * Namespace: 
 * Java type: de.grogra.ext.exchangegraph.xmlbeans.Property
 *
 * Automatically generated - do not modify.
 */
package de.grogra.ext.exchangegraph.xmlbeans.impl;

import javax.xml.namespace.QName;
import org.apache.xmlbeans.QNameSet;
import org.apache.xmlbeans.XmlObject;

/**
 * An XML Property(@).
 *
 * This is a complex type.
 */
public class PropertyImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements de.grogra.ext.exchangegraph.xmlbeans.Property {
    private static final long serialVersionUID = 1L;

    public PropertyImpl(org.apache.xmlbeans.SchemaType sType) {
        super(sType);
    }

    private static final QName[] PROPERTY_QNAME = {
        new QName("", "matrix"),
        new QName("", "rgb"),
        new QName("", "rgba"),
        new QName("", "list_of_int"),
        new QName("", "list_of_float"),
        new QName("", "name"),
        new QName("", "value"),
        new QName("", "type"),
    };


    /**
     * Gets the "matrix" element
     */
    @Override
    public java.util.List getMatrix() {
        synchronized (monitor()) {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PROPERTY_QNAME[0], 0);
            return (target == null) ? null : target.getListValue();
        }
    }

    /**
     * Gets (as xml) the "matrix" element
     */
    @Override
    public de.grogra.ext.exchangegraph.xmlbeans.MatrixType xgetMatrix() {
        synchronized (monitor()) {
            check_orphaned();
            de.grogra.ext.exchangegraph.xmlbeans.MatrixType target = null;
            target = (de.grogra.ext.exchangegraph.xmlbeans.MatrixType)get_store().find_element_user(PROPERTY_QNAME[0], 0);
            return target;
        }
    }

    /**
     * True if has "matrix" element
     */
    @Override
    public boolean isSetMatrix() {
        synchronized (monitor()) {
            check_orphaned();
            return get_store().count_elements(PROPERTY_QNAME[0]) != 0;
        }
    }

    /**
     * Sets the "matrix" element
     */
    @Override
    public void setMatrix(java.util.List matrix) {
        synchronized (monitor()) {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PROPERTY_QNAME[0], 0);
            if (target == null) {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(PROPERTY_QNAME[0]);
            }
            target.setListValue(matrix);
        }
    }

    /**
     * Sets (as xml) the "matrix" element
     */
    @Override
    public void xsetMatrix(de.grogra.ext.exchangegraph.xmlbeans.MatrixType matrix) {
        synchronized (monitor()) {
            check_orphaned();
            de.grogra.ext.exchangegraph.xmlbeans.MatrixType target = null;
            target = (de.grogra.ext.exchangegraph.xmlbeans.MatrixType)get_store().find_element_user(PROPERTY_QNAME[0], 0);
            if (target == null) {
                target = (de.grogra.ext.exchangegraph.xmlbeans.MatrixType)get_store().add_element_user(PROPERTY_QNAME[0]);
            }
            target.set(matrix);
        }
    }

    /**
     * Unsets the "matrix" element
     */
    @Override
    public void unsetMatrix() {
        synchronized (monitor()) {
            check_orphaned();
            get_store().remove_element(PROPERTY_QNAME[0], 0);
        }
    }

    /**
     * Gets the "rgb" element
     */
    @Override
    public java.util.List getRgb() {
        synchronized (monitor()) {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PROPERTY_QNAME[1], 0);
            return (target == null) ? null : target.getListValue();
        }
    }

    /**
     * Gets (as xml) the "rgb" element
     */
    @Override
    public de.grogra.ext.exchangegraph.xmlbeans.RgbType xgetRgb() {
        synchronized (monitor()) {
            check_orphaned();
            de.grogra.ext.exchangegraph.xmlbeans.RgbType target = null;
            target = (de.grogra.ext.exchangegraph.xmlbeans.RgbType)get_store().find_element_user(PROPERTY_QNAME[1], 0);
            return target;
        }
    }

    /**
     * True if has "rgb" element
     */
    @Override
    public boolean isSetRgb() {
        synchronized (monitor()) {
            check_orphaned();
            return get_store().count_elements(PROPERTY_QNAME[1]) != 0;
        }
    }

    /**
     * Sets the "rgb" element
     */
    @Override
    public void setRgb(java.util.List rgb) {
        synchronized (monitor()) {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PROPERTY_QNAME[1], 0);
            if (target == null) {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(PROPERTY_QNAME[1]);
            }
            target.setListValue(rgb);
        }
    }

    /**
     * Sets (as xml) the "rgb" element
     */
    @Override
    public void xsetRgb(de.grogra.ext.exchangegraph.xmlbeans.RgbType rgb) {
        synchronized (monitor()) {
            check_orphaned();
            de.grogra.ext.exchangegraph.xmlbeans.RgbType target = null;
            target = (de.grogra.ext.exchangegraph.xmlbeans.RgbType)get_store().find_element_user(PROPERTY_QNAME[1], 0);
            if (target == null) {
                target = (de.grogra.ext.exchangegraph.xmlbeans.RgbType)get_store().add_element_user(PROPERTY_QNAME[1]);
            }
            target.set(rgb);
        }
    }

    /**
     * Unsets the "rgb" element
     */
    @Override
    public void unsetRgb() {
        synchronized (monitor()) {
            check_orphaned();
            get_store().remove_element(PROPERTY_QNAME[1], 0);
        }
    }

    /**
     * Gets the "rgba" element
     */
    @Override
    public java.util.List getRgba() {
        synchronized (monitor()) {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PROPERTY_QNAME[2], 0);
            return (target == null) ? null : target.getListValue();
        }
    }

    /**
     * Gets (as xml) the "rgba" element
     */
    @Override
    public de.grogra.ext.exchangegraph.xmlbeans.RgbaType xgetRgba() {
        synchronized (monitor()) {
            check_orphaned();
            de.grogra.ext.exchangegraph.xmlbeans.RgbaType target = null;
            target = (de.grogra.ext.exchangegraph.xmlbeans.RgbaType)get_store().find_element_user(PROPERTY_QNAME[2], 0);
            return target;
        }
    }

    /**
     * True if has "rgba" element
     */
    @Override
    public boolean isSetRgba() {
        synchronized (monitor()) {
            check_orphaned();
            return get_store().count_elements(PROPERTY_QNAME[2]) != 0;
        }
    }

    /**
     * Sets the "rgba" element
     */
    @Override
    public void setRgba(java.util.List rgba) {
        synchronized (monitor()) {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PROPERTY_QNAME[2], 0);
            if (target == null) {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(PROPERTY_QNAME[2]);
            }
            target.setListValue(rgba);
        }
    }

    /**
     * Sets (as xml) the "rgba" element
     */
    @Override
    public void xsetRgba(de.grogra.ext.exchangegraph.xmlbeans.RgbaType rgba) {
        synchronized (monitor()) {
            check_orphaned();
            de.grogra.ext.exchangegraph.xmlbeans.RgbaType target = null;
            target = (de.grogra.ext.exchangegraph.xmlbeans.RgbaType)get_store().find_element_user(PROPERTY_QNAME[2], 0);
            if (target == null) {
                target = (de.grogra.ext.exchangegraph.xmlbeans.RgbaType)get_store().add_element_user(PROPERTY_QNAME[2]);
            }
            target.set(rgba);
        }
    }

    /**
     * Unsets the "rgba" element
     */
    @Override
    public void unsetRgba() {
        synchronized (monitor()) {
            check_orphaned();
            get_store().remove_element(PROPERTY_QNAME[2], 0);
        }
    }

    /**
     * Gets the "list_of_int" element
     */
    @Override
    public java.util.List getListOfInt() {
        synchronized (monitor()) {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PROPERTY_QNAME[3], 0);
            return (target == null) ? null : target.getListValue();
        }
    }

    /**
     * Gets (as xml) the "list_of_int" element
     */
    @Override
    public de.grogra.ext.exchangegraph.xmlbeans.ListOfInt xgetListOfInt() {
        synchronized (monitor()) {
            check_orphaned();
            de.grogra.ext.exchangegraph.xmlbeans.ListOfInt target = null;
            target = (de.grogra.ext.exchangegraph.xmlbeans.ListOfInt)get_store().find_element_user(PROPERTY_QNAME[3], 0);
            return target;
        }
    }

    /**
     * True if has "list_of_int" element
     */
    @Override
    public boolean isSetListOfInt() {
        synchronized (monitor()) {
            check_orphaned();
            return get_store().count_elements(PROPERTY_QNAME[3]) != 0;
        }
    }

    /**
     * Sets the "list_of_int" element
     */
    @Override
    public void setListOfInt(java.util.List listOfInt) {
        synchronized (monitor()) {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PROPERTY_QNAME[3], 0);
            if (target == null) {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(PROPERTY_QNAME[3]);
            }
            target.setListValue(listOfInt);
        }
    }

    /**
     * Sets (as xml) the "list_of_int" element
     */
    @Override
    public void xsetListOfInt(de.grogra.ext.exchangegraph.xmlbeans.ListOfInt listOfInt) {
        synchronized (monitor()) {
            check_orphaned();
            de.grogra.ext.exchangegraph.xmlbeans.ListOfInt target = null;
            target = (de.grogra.ext.exchangegraph.xmlbeans.ListOfInt)get_store().find_element_user(PROPERTY_QNAME[3], 0);
            if (target == null) {
                target = (de.grogra.ext.exchangegraph.xmlbeans.ListOfInt)get_store().add_element_user(PROPERTY_QNAME[3]);
            }
            target.set(listOfInt);
        }
    }

    /**
     * Unsets the "list_of_int" element
     */
    @Override
    public void unsetListOfInt() {
        synchronized (monitor()) {
            check_orphaned();
            get_store().remove_element(PROPERTY_QNAME[3], 0);
        }
    }

    /**
     * Gets the "list_of_float" element
     */
    @Override
    public java.util.List getListOfFloat() {
        synchronized (monitor()) {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PROPERTY_QNAME[4], 0);
            return (target == null) ? null : target.getListValue();
        }
    }

    /**
     * Gets (as xml) the "list_of_float" element
     */
    @Override
    public de.grogra.ext.exchangegraph.xmlbeans.ListOfFloat xgetListOfFloat() {
        synchronized (monitor()) {
            check_orphaned();
            de.grogra.ext.exchangegraph.xmlbeans.ListOfFloat target = null;
            target = (de.grogra.ext.exchangegraph.xmlbeans.ListOfFloat)get_store().find_element_user(PROPERTY_QNAME[4], 0);
            return target;
        }
    }

    /**
     * True if has "list_of_float" element
     */
    @Override
    public boolean isSetListOfFloat() {
        synchronized (monitor()) {
            check_orphaned();
            return get_store().count_elements(PROPERTY_QNAME[4]) != 0;
        }
    }

    /**
     * Sets the "list_of_float" element
     */
    @Override
    public void setListOfFloat(java.util.List listOfFloat) {
        synchronized (monitor()) {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PROPERTY_QNAME[4], 0);
            if (target == null) {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(PROPERTY_QNAME[4]);
            }
            target.setListValue(listOfFloat);
        }
    }

    /**
     * Sets (as xml) the "list_of_float" element
     */
    @Override
    public void xsetListOfFloat(de.grogra.ext.exchangegraph.xmlbeans.ListOfFloat listOfFloat) {
        synchronized (monitor()) {
            check_orphaned();
            de.grogra.ext.exchangegraph.xmlbeans.ListOfFloat target = null;
            target = (de.grogra.ext.exchangegraph.xmlbeans.ListOfFloat)get_store().find_element_user(PROPERTY_QNAME[4], 0);
            if (target == null) {
                target = (de.grogra.ext.exchangegraph.xmlbeans.ListOfFloat)get_store().add_element_user(PROPERTY_QNAME[4]);
            }
            target.set(listOfFloat);
        }
    }

    /**
     * Unsets the "list_of_float" element
     */
    @Override
    public void unsetListOfFloat() {
        synchronized (monitor()) {
            check_orphaned();
            get_store().remove_element(PROPERTY_QNAME[4], 0);
        }
    }

    /**
     * Gets the "name" attribute
     */
    @Override
    public java.lang.String getName() {
        synchronized (monitor()) {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_attribute_user(PROPERTY_QNAME[5]);
            return (target == null) ? null : target.getStringValue();
        }
    }

    /**
     * Gets (as xml) the "name" attribute
     */
    @Override
    public org.apache.xmlbeans.XmlString xgetName() {
        synchronized (monitor()) {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_attribute_user(PROPERTY_QNAME[5]);
            return target;
        }
    }

    /**
     * Sets the "name" attribute
     */
    @Override
    public void setName(java.lang.String name) {
        synchronized (monitor()) {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_attribute_user(PROPERTY_QNAME[5]);
            if (target == null) {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_attribute_user(PROPERTY_QNAME[5]);
            }
            target.setStringValue(name);
        }
    }

    /**
     * Sets (as xml) the "name" attribute
     */
    @Override
    public void xsetName(org.apache.xmlbeans.XmlString name) {
        synchronized (monitor()) {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_attribute_user(PROPERTY_QNAME[5]);
            if (target == null) {
                target = (org.apache.xmlbeans.XmlString)get_store().add_attribute_user(PROPERTY_QNAME[5]);
            }
            target.set(name);
        }
    }

    /**
     * Gets the "value" attribute
     */
    @Override
    public java.lang.String getValue() {
        synchronized (monitor()) {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_attribute_user(PROPERTY_QNAME[6]);
            return (target == null) ? null : target.getStringValue();
        }
    }

    /**
     * Gets (as xml) the "value" attribute
     */
    @Override
    public org.apache.xmlbeans.XmlString xgetValue() {
        synchronized (monitor()) {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_attribute_user(PROPERTY_QNAME[6]);
            return target;
        }
    }

    /**
     * True if has "value" attribute
     */
    @Override
    public boolean isSetValue() {
        synchronized (monitor()) {
            check_orphaned();
            return get_store().find_attribute_user(PROPERTY_QNAME[6]) != null;
        }
    }

    /**
     * Sets the "value" attribute
     */
    @Override
    public void setValue(java.lang.String value) {
        synchronized (monitor()) {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_attribute_user(PROPERTY_QNAME[6]);
            if (target == null) {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_attribute_user(PROPERTY_QNAME[6]);
            }
            target.setStringValue(value);
        }
    }

    /**
     * Sets (as xml) the "value" attribute
     */
    @Override
    public void xsetValue(org.apache.xmlbeans.XmlString value) {
        synchronized (monitor()) {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_attribute_user(PROPERTY_QNAME[6]);
            if (target == null) {
                target = (org.apache.xmlbeans.XmlString)get_store().add_attribute_user(PROPERTY_QNAME[6]);
            }
            target.set(value);
        }
    }

    /**
     * Unsets the "value" attribute
     */
    @Override
    public void unsetValue() {
        synchronized (monitor()) {
            check_orphaned();
            get_store().remove_attribute(PROPERTY_QNAME[6]);
        }
    }

    /**
     * Gets the "type" attribute
     */
    @Override
    public java.lang.String getType() {
        synchronized (monitor()) {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_attribute_user(PROPERTY_QNAME[7]);
            return (target == null) ? null : target.getStringValue();
        }
    }

    /**
     * Gets (as xml) the "type" attribute
     */
    @Override
    public org.apache.xmlbeans.XmlString xgetType() {
        synchronized (monitor()) {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_attribute_user(PROPERTY_QNAME[7]);
            return target;
        }
    }

    /**
     * True if has "type" attribute
     */
    @Override
    public boolean isSetType() {
        synchronized (monitor()) {
            check_orphaned();
            return get_store().find_attribute_user(PROPERTY_QNAME[7]) != null;
        }
    }

    /**
     * Sets the "type" attribute
     */
    @Override
    public void setType(java.lang.String type) {
        synchronized (monitor()) {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_attribute_user(PROPERTY_QNAME[7]);
            if (target == null) {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_attribute_user(PROPERTY_QNAME[7]);
            }
            target.setStringValue(type);
        }
    }

    /**
     * Sets (as xml) the "type" attribute
     */
    @Override
    public void xsetType(org.apache.xmlbeans.XmlString type) {
        synchronized (monitor()) {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_attribute_user(PROPERTY_QNAME[7]);
            if (target == null) {
                target = (org.apache.xmlbeans.XmlString)get_store().add_attribute_user(PROPERTY_QNAME[7]);
            }
            target.set(type);
        }
    }

    /**
     * Unsets the "type" attribute
     */
    @Override
    public void unsetType() {
        synchronized (monitor()) {
            check_orphaned();
            get_store().remove_attribute(PROPERTY_QNAME[7]);
        }
    }
}
