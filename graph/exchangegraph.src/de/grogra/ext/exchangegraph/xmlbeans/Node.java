/*
 * XML Type:  Node
 * Namespace: 
 * Java type: de.grogra.ext.exchangegraph.xmlbeans.Node
 *
 * Automatically generated - do not modify.
 */
package de.grogra.ext.exchangegraph.xmlbeans;

import org.apache.xmlbeans.impl.schema.ElementFactory;
import org.apache.xmlbeans.impl.schema.AbstractDocumentFactory;
import org.apache.xmlbeans.impl.schema.DocumentFactory;
import org.apache.xmlbeans.impl.schema.SimpleTypeFactory;


/**
 * An XML Node(@).
 *
 * This is a complex type.
 */
public interface Node extends org.apache.xmlbeans.XmlObject {
    DocumentFactory<de.grogra.ext.exchangegraph.xmlbeans.Node> Factory = new DocumentFactory<>(org.apache.xmlbeans.metadata.system.sE6B7510D6B33B7EC58169E8DC7B33B7F.TypeSystemHolder.typeSystem, "noded00btype");
    org.apache.xmlbeans.SchemaType type = Factory.getType();


    /**
     * Gets a List of "property" elements
     */
    java.util.List<de.grogra.ext.exchangegraph.xmlbeans.Property> getPropertyList();

    /**
     * Gets array of all "property" elements
     */
    de.grogra.ext.exchangegraph.xmlbeans.Property[] getPropertyArray();

    /**
     * Gets ith "property" element
     */
    de.grogra.ext.exchangegraph.xmlbeans.Property getPropertyArray(int i);

    /**
     * Returns number of "property" element
     */
    int sizeOfPropertyArray();

    /**
     * Sets array of all "property" element
     */
    void setPropertyArray(de.grogra.ext.exchangegraph.xmlbeans.Property[] propertyArray);

    /**
     * Sets ith "property" element
     */
    void setPropertyArray(int i, de.grogra.ext.exchangegraph.xmlbeans.Property property);

    /**
     * Inserts and returns a new empty value (as xml) as the ith "property" element
     */
    de.grogra.ext.exchangegraph.xmlbeans.Property insertNewProperty(int i);

    /**
     * Appends and returns a new empty value (as xml) as the last "property" element
     */
    de.grogra.ext.exchangegraph.xmlbeans.Property addNewProperty();

    /**
     * Removes the ith "property" element
     */
    void removeProperty(int i);

    /**
     * Gets the "id" attribute
     */
    java.lang.String getId();

    /**
     * Gets (as xml) the "id" attribute
     */
    de.grogra.ext.exchangegraph.xmlbeans.IdType xgetId();

    /**
     * Sets the "id" attribute
     */
    void setId(java.lang.String id);

    /**
     * Sets (as xml) the "id" attribute
     */
    void xsetId(de.grogra.ext.exchangegraph.xmlbeans.IdType id);

    /**
     * Gets the "type" attribute
     */
    java.lang.String getType();

    /**
     * Gets (as xml) the "type" attribute
     */
    org.apache.xmlbeans.XmlString xgetType();

    /**
     * True if has "type" attribute
     */
    boolean isSetType();

    /**
     * Sets the "type" attribute
     */
    void setType(java.lang.String type);

    /**
     * Sets (as xml) the "type" attribute
     */
    void xsetType(org.apache.xmlbeans.XmlString type);

    /**
     * Unsets the "type" attribute
     */
    void unsetType();

    /**
     * Gets the "name" attribute
     */
    java.lang.String getName();

    /**
     * Gets (as xml) the "name" attribute
     */
    org.apache.xmlbeans.XmlString xgetName();

    /**
     * True if has "name" attribute
     */
    boolean isSetName();

    /**
     * Sets the "name" attribute
     */
    void setName(java.lang.String name);

    /**
     * Sets (as xml) the "name" attribute
     */
    void xsetName(org.apache.xmlbeans.XmlString name);

    /**
     * Unsets the "name" attribute
     */
    void unsetName();
}
