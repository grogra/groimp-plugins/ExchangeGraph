/*
 * XML Type:  ImplementsType
 * Namespace: 
 * Java type: de.grogra.ext.exchangegraph.xmlbeans.ImplementsType
 *
 * Automatically generated - do not modify.
 */
package de.grogra.ext.exchangegraph.xmlbeans;

import org.apache.xmlbeans.impl.schema.ElementFactory;
import org.apache.xmlbeans.impl.schema.AbstractDocumentFactory;
import org.apache.xmlbeans.impl.schema.DocumentFactory;
import org.apache.xmlbeans.impl.schema.SimpleTypeFactory;


/**
 * An XML ImplementsType(@).
 *
 * This is a complex type.
 */
public interface ImplementsType extends org.apache.xmlbeans.XmlObject {
    DocumentFactory<de.grogra.ext.exchangegraph.xmlbeans.ImplementsType> Factory = new DocumentFactory<>(org.apache.xmlbeans.metadata.system.sE6B7510D6B33B7EC58169E8DC7B33B7F.TypeSystemHolder.typeSystem, "implementstypeedd3type");
    org.apache.xmlbeans.SchemaType type = Factory.getType();


    /**
     * Gets the "name" attribute
     */
    java.lang.String getName();

    /**
     * Gets (as xml) the "name" attribute
     */
    org.apache.xmlbeans.XmlString xgetName();

    /**
     * True if has "name" attribute
     */
    boolean isSetName();

    /**
     * Sets the "name" attribute
     */
    void setName(java.lang.String name);

    /**
     * Sets (as xml) the "name" attribute
     */
    void xsetName(org.apache.xmlbeans.XmlString name);

    /**
     * Unsets the "name" attribute
     */
    void unsetName();
}
