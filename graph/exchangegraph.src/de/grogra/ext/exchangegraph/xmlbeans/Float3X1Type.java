/*
 * XML Type:  float3x1_type
 * Namespace: 
 * Java type: de.grogra.ext.exchangegraph.xmlbeans.Float3X1Type
 *
 * Automatically generated - do not modify.
 */
package de.grogra.ext.exchangegraph.xmlbeans;

import org.apache.xmlbeans.impl.schema.ElementFactory;
import org.apache.xmlbeans.impl.schema.AbstractDocumentFactory;
import org.apache.xmlbeans.impl.schema.DocumentFactory;
import org.apache.xmlbeans.impl.schema.SimpleTypeFactory;


/**
 * An XML float3x1_type(@).
 *
 * This is a list type whose items are de.grogra.ext.exchangegraph.xmlbeans.FloatType.
 */
public interface Float3X1Type extends de.grogra.ext.exchangegraph.xmlbeans.ListOfFloatType {
    java.util.List getListValue();
    java.util.List xgetListValue();
    void setListValue(java.util.List<?> list);
    SimpleTypeFactory<de.grogra.ext.exchangegraph.xmlbeans.Float3X1Type> Factory = new SimpleTypeFactory<>(org.apache.xmlbeans.metadata.system.sE6B7510D6B33B7EC58169E8DC7B33B7F.TypeSystemHolder.typeSystem, "float3x1type6400type");
    org.apache.xmlbeans.SchemaType type = Factory.getType();

}
