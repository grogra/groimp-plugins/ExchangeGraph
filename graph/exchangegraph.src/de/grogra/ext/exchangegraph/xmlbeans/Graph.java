/*
 * XML Type:  Graph
 * Namespace: 
 * Java type: de.grogra.ext.exchangegraph.xmlbeans.Graph
 *
 * Automatically generated - do not modify.
 */
package de.grogra.ext.exchangegraph.xmlbeans;

import org.apache.xmlbeans.impl.schema.ElementFactory;
import org.apache.xmlbeans.impl.schema.AbstractDocumentFactory;
import org.apache.xmlbeans.impl.schema.DocumentFactory;
import org.apache.xmlbeans.impl.schema.SimpleTypeFactory;


/**
 * An XML Graph(@).
 *
 * This is a complex type.
 */
public interface Graph extends org.apache.xmlbeans.XmlObject {
    DocumentFactory<de.grogra.ext.exchangegraph.xmlbeans.Graph> Factory = new DocumentFactory<>(org.apache.xmlbeans.metadata.system.sE6B7510D6B33B7EC58169E8DC7B33B7F.TypeSystemHolder.typeSystem, "graphe065type");
    org.apache.xmlbeans.SchemaType type = Factory.getType();


    /**
     * Gets a List of "type" elements
     */
    java.util.List<de.grogra.ext.exchangegraph.xmlbeans.Type> getTypeList();

    /**
     * Gets array of all "type" elements
     */
    de.grogra.ext.exchangegraph.xmlbeans.Type[] getTypeArray();

    /**
     * Gets ith "type" element
     */
    de.grogra.ext.exchangegraph.xmlbeans.Type getTypeArray(int i);

    /**
     * Returns number of "type" element
     */
    int sizeOfTypeArray();

    /**
     * Sets array of all "type" element
     */
    void setTypeArray(de.grogra.ext.exchangegraph.xmlbeans.Type[] typeArray);

    /**
     * Sets ith "type" element
     */
    void setTypeArray(int i, de.grogra.ext.exchangegraph.xmlbeans.Type type);

    /**
     * Inserts and returns a new empty value (as xml) as the ith "type" element
     */
    de.grogra.ext.exchangegraph.xmlbeans.Type insertNewType(int i);

    /**
     * Appends and returns a new empty value (as xml) as the last "type" element
     */
    de.grogra.ext.exchangegraph.xmlbeans.Type addNewType();

    /**
     * Removes the ith "type" element
     */
    void removeType(int i);

    /**
     * Gets a List of "root" elements
     */
    java.util.List<de.grogra.ext.exchangegraph.xmlbeans.Root> getRootList();

    /**
     * Gets array of all "root" elements
     */
    de.grogra.ext.exchangegraph.xmlbeans.Root[] getRootArray();

    /**
     * Gets ith "root" element
     */
    de.grogra.ext.exchangegraph.xmlbeans.Root getRootArray(int i);

    /**
     * Returns number of "root" element
     */
    int sizeOfRootArray();

    /**
     * Sets array of all "root" element
     */
    void setRootArray(de.grogra.ext.exchangegraph.xmlbeans.Root[] rootArray);

    /**
     * Sets ith "root" element
     */
    void setRootArray(int i, de.grogra.ext.exchangegraph.xmlbeans.Root root);

    /**
     * Inserts and returns a new empty value (as xml) as the ith "root" element
     */
    de.grogra.ext.exchangegraph.xmlbeans.Root insertNewRoot(int i);

    /**
     * Appends and returns a new empty value (as xml) as the last "root" element
     */
    de.grogra.ext.exchangegraph.xmlbeans.Root addNewRoot();

    /**
     * Removes the ith "root" element
     */
    void removeRoot(int i);

    /**
     * Gets a List of "node" elements
     */
    java.util.List<de.grogra.ext.exchangegraph.xmlbeans.Node> getNodeList();

    /**
     * Gets array of all "node" elements
     */
    de.grogra.ext.exchangegraph.xmlbeans.Node[] getNodeArray();

    /**
     * Gets ith "node" element
     */
    de.grogra.ext.exchangegraph.xmlbeans.Node getNodeArray(int i);

    /**
     * Returns number of "node" element
     */
    int sizeOfNodeArray();

    /**
     * Sets array of all "node" element
     */
    void setNodeArray(de.grogra.ext.exchangegraph.xmlbeans.Node[] nodeArray);

    /**
     * Sets ith "node" element
     */
    void setNodeArray(int i, de.grogra.ext.exchangegraph.xmlbeans.Node node);

    /**
     * Inserts and returns a new empty value (as xml) as the ith "node" element
     */
    de.grogra.ext.exchangegraph.xmlbeans.Node insertNewNode(int i);

    /**
     * Appends and returns a new empty value (as xml) as the last "node" element
     */
    de.grogra.ext.exchangegraph.xmlbeans.Node addNewNode();

    /**
     * Removes the ith "node" element
     */
    void removeNode(int i);

    /**
     * Gets a List of "edge" elements
     */
    java.util.List<de.grogra.ext.exchangegraph.xmlbeans.Edge> getEdgeList();

    /**
     * Gets array of all "edge" elements
     */
    de.grogra.ext.exchangegraph.xmlbeans.Edge[] getEdgeArray();

    /**
     * Gets ith "edge" element
     */
    de.grogra.ext.exchangegraph.xmlbeans.Edge getEdgeArray(int i);

    /**
     * Returns number of "edge" element
     */
    int sizeOfEdgeArray();

    /**
     * Sets array of all "edge" element
     */
    void setEdgeArray(de.grogra.ext.exchangegraph.xmlbeans.Edge[] edgeArray);

    /**
     * Sets ith "edge" element
     */
    void setEdgeArray(int i, de.grogra.ext.exchangegraph.xmlbeans.Edge edge);

    /**
     * Inserts and returns a new empty value (as xml) as the ith "edge" element
     */
    de.grogra.ext.exchangegraph.xmlbeans.Edge insertNewEdge(int i);

    /**
     * Appends and returns a new empty value (as xml) as the last "edge" element
     */
    de.grogra.ext.exchangegraph.xmlbeans.Edge addNewEdge();

    /**
     * Removes the ith "edge" element
     */
    void removeEdge(int i);
}
