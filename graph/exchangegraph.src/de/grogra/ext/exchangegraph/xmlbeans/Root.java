/*
 * XML Type:  Root
 * Namespace: 
 * Java type: de.grogra.ext.exchangegraph.xmlbeans.Root
 *
 * Automatically generated - do not modify.
 */
package de.grogra.ext.exchangegraph.xmlbeans;

import org.apache.xmlbeans.impl.schema.ElementFactory;
import org.apache.xmlbeans.impl.schema.AbstractDocumentFactory;
import org.apache.xmlbeans.impl.schema.DocumentFactory;
import org.apache.xmlbeans.impl.schema.SimpleTypeFactory;


/**
 * An XML Root(@).
 *
 * This is a complex type.
 */
public interface Root extends org.apache.xmlbeans.XmlObject {
    DocumentFactory<de.grogra.ext.exchangegraph.xmlbeans.Root> Factory = new DocumentFactory<>(org.apache.xmlbeans.metadata.system.sE6B7510D6B33B7EC58169E8DC7B33B7F.TypeSystemHolder.typeSystem, "roota2ebtype");
    org.apache.xmlbeans.SchemaType type = Factory.getType();


    /**
     * Gets the "root_id" attribute
     */
    java.lang.String getRootId();

    /**
     * Gets (as xml) the "root_id" attribute
     */
    de.grogra.ext.exchangegraph.xmlbeans.IdType xgetRootId();

    /**
     * Sets the "root_id" attribute
     */
    void setRootId(java.lang.String rootId);

    /**
     * Sets (as xml) the "root_id" attribute
     */
    void xsetRootId(de.grogra.ext.exchangegraph.xmlbeans.IdType rootId);
}
