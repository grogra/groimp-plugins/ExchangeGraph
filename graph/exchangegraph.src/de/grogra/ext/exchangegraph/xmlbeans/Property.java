/*
 * XML Type:  Property
 * Namespace: 
 * Java type: de.grogra.ext.exchangegraph.xmlbeans.Property
 *
 * Automatically generated - do not modify.
 */
package de.grogra.ext.exchangegraph.xmlbeans;

import org.apache.xmlbeans.impl.schema.ElementFactory;
import org.apache.xmlbeans.impl.schema.AbstractDocumentFactory;
import org.apache.xmlbeans.impl.schema.DocumentFactory;
import org.apache.xmlbeans.impl.schema.SimpleTypeFactory;


/**
 * An XML Property(@).
 *
 * This is a complex type.
 */
public interface Property extends org.apache.xmlbeans.XmlObject {
    DocumentFactory<de.grogra.ext.exchangegraph.xmlbeans.Property> Factory = new DocumentFactory<>(org.apache.xmlbeans.metadata.system.sE6B7510D6B33B7EC58169E8DC7B33B7F.TypeSystemHolder.typeSystem, "propertyd35etype");
    org.apache.xmlbeans.SchemaType type = Factory.getType();


    /**
     * Gets the "matrix" element
     */
    java.util.List getMatrix();

    /**
     * Gets (as xml) the "matrix" element
     */
    de.grogra.ext.exchangegraph.xmlbeans.MatrixType xgetMatrix();

    /**
     * True if has "matrix" element
     */
    boolean isSetMatrix();

    /**
     * Sets the "matrix" element
     */
    void setMatrix(java.util.List matrix);

    /**
     * Sets (as xml) the "matrix" element
     */
    void xsetMatrix(de.grogra.ext.exchangegraph.xmlbeans.MatrixType matrix);

    /**
     * Unsets the "matrix" element
     */
    void unsetMatrix();

    /**
     * Gets the "rgb" element
     */
    java.util.List getRgb();

    /**
     * Gets (as xml) the "rgb" element
     */
    de.grogra.ext.exchangegraph.xmlbeans.RgbType xgetRgb();

    /**
     * True if has "rgb" element
     */
    boolean isSetRgb();

    /**
     * Sets the "rgb" element
     */
    void setRgb(java.util.List rgb);

    /**
     * Sets (as xml) the "rgb" element
     */
    void xsetRgb(de.grogra.ext.exchangegraph.xmlbeans.RgbType rgb);

    /**
     * Unsets the "rgb" element
     */
    void unsetRgb();

    /**
     * Gets the "rgba" element
     */
    java.util.List getRgba();

    /**
     * Gets (as xml) the "rgba" element
     */
    de.grogra.ext.exchangegraph.xmlbeans.RgbaType xgetRgba();

    /**
     * True if has "rgba" element
     */
    boolean isSetRgba();

    /**
     * Sets the "rgba" element
     */
    void setRgba(java.util.List rgba);

    /**
     * Sets (as xml) the "rgba" element
     */
    void xsetRgba(de.grogra.ext.exchangegraph.xmlbeans.RgbaType rgba);

    /**
     * Unsets the "rgba" element
     */
    void unsetRgba();

    /**
     * Gets the "list_of_int" element
     */
    java.util.List getListOfInt();

    /**
     * Gets (as xml) the "list_of_int" element
     */
    de.grogra.ext.exchangegraph.xmlbeans.ListOfInt xgetListOfInt();

    /**
     * True if has "list_of_int" element
     */
    boolean isSetListOfInt();

    /**
     * Sets the "list_of_int" element
     */
    void setListOfInt(java.util.List listOfInt);

    /**
     * Sets (as xml) the "list_of_int" element
     */
    void xsetListOfInt(de.grogra.ext.exchangegraph.xmlbeans.ListOfInt listOfInt);

    /**
     * Unsets the "list_of_int" element
     */
    void unsetListOfInt();

    /**
     * Gets the "list_of_float" element
     */
    java.util.List getListOfFloat();

    /**
     * Gets (as xml) the "list_of_float" element
     */
    de.grogra.ext.exchangegraph.xmlbeans.ListOfFloat xgetListOfFloat();

    /**
     * True if has "list_of_float" element
     */
    boolean isSetListOfFloat();

    /**
     * Sets the "list_of_float" element
     */
    void setListOfFloat(java.util.List listOfFloat);

    /**
     * Sets (as xml) the "list_of_float" element
     */
    void xsetListOfFloat(de.grogra.ext.exchangegraph.xmlbeans.ListOfFloat listOfFloat);

    /**
     * Unsets the "list_of_float" element
     */
    void unsetListOfFloat();

    /**
     * Gets the "name" attribute
     */
    java.lang.String getName();

    /**
     * Gets (as xml) the "name" attribute
     */
    org.apache.xmlbeans.XmlString xgetName();

    /**
     * Sets the "name" attribute
     */
    void setName(java.lang.String name);

    /**
     * Sets (as xml) the "name" attribute
     */
    void xsetName(org.apache.xmlbeans.XmlString name);

    /**
     * Gets the "value" attribute
     */
    java.lang.String getValue();

    /**
     * Gets (as xml) the "value" attribute
     */
    org.apache.xmlbeans.XmlString xgetValue();

    /**
     * True if has "value" attribute
     */
    boolean isSetValue();

    /**
     * Sets the "value" attribute
     */
    void setValue(java.lang.String value);

    /**
     * Sets (as xml) the "value" attribute
     */
    void xsetValue(org.apache.xmlbeans.XmlString value);

    /**
     * Unsets the "value" attribute
     */
    void unsetValue();

    /**
     * Gets the "type" attribute
     */
    java.lang.String getType();

    /**
     * Gets (as xml) the "type" attribute
     */
    org.apache.xmlbeans.XmlString xgetType();

    /**
     * True if has "type" attribute
     */
    boolean isSetType();

    /**
     * Sets the "type" attribute
     */
    void setType(java.lang.String type);

    /**
     * Sets (as xml) the "type" attribute
     */
    void xsetType(org.apache.xmlbeans.XmlString type);

    /**
     * Unsets the "type" attribute
     */
    void unsetType();
}
