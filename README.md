# Exchange Graph

Support interface of exchange graph object. Exchange graphs are graph that describe the same amount of topological and geometrical data as GroIMP graph. They can be used as intermediate graph to export/import sub graph between GroIMP projects. 

During the export, Exchange graphs serialize the instanciation rules to add them to the graph.

It includes support of the file types (for import & export):
- ".xeg" 
